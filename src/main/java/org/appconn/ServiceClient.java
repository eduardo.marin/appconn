package org.appconn;

import org.appconn.channel.AbstractClient;
import org.appconn.mgmt.Client;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <i>appconn</i> ServiceClient.
 */
public abstract class ServiceClient {

    private final Client.CONNS_TYPE connsType;

    private volatile SortedSet<AbstractConn> conns;
    private Iterator<AbstractConn> connIterator;
    private final Lock connIteratorLock;

    private volatile Map<Integer, SortedSet<AbstractConn>> connsMap;

    private final ConnGetter connGetter;
    private final int connectTimeout;

    static final Predicate<ServiceClient> IS_CONN = serviceClient -> serviceClient.getConnsType() == Client.CONNS_TYPE.CONN;

    protected ServiceClient(byte connsType, ConnGetter connGetter, int connectTimeout) {
        this.connsType = Client.CONNS_TYPE.valueOf(connsType).orElse(null);
        connIteratorLock = new ReentrantLock();
        setConns(Collections.emptySortedSet());
        this.connGetter = connGetter;
        this.connectTimeout = connectTimeout;
    }

    protected AbstractClient getClient(int methodKey, Object... args) throws AppconnException {
        return connGetter.getConn(this, methodKey, args)
                         .orElseThrow(() -> new AppconnException("conn is not present"))
                         .getClient();
    }

    Client.CONNS_TYPE getConnsType() {
        return connsType;
    }

    Optional<AbstractConn> first() {
        try {
            return Optional.of(conns.first());
        } catch (NoSuchElementException e) {
            return Optional.empty();
        }
    }

    Optional<AbstractConn> next() {
        connIteratorLock.lock();
        try {
            if (connIterator.hasNext()) return Optional.of(connIterator.next());
            else {
                connIterator = conns.iterator();
                if (connIterator.hasNext()) return Optional.of(connIterator.next());
            }
        } finally {
            connIteratorLock.unlock();
        }
        return Optional.empty();
    }

    SortedSet<AbstractConn> getConns() {
        return conns;
    }

    void setConns(SortedSet<AbstractConn> conns) {
        connIteratorLock.lock();
        this.conns = conns;
        try {
            connIterator = this.conns.iterator();
            connsMap = this.conns.stream()
                                 .collect(Collectors.groupingBy(AbstractConn::getMappingKey, Collectors.toCollection(TreeSet::new)));
        } finally {
            connIteratorLock.unlock();
        }
    }

    /**
     * @return the AbstractConn map
     */
    public Map<Integer, SortedSet<AbstractConn>> getConnsMap() {
        return connsMap;
    }

    int getConnectTimeout() {
        return connectTimeout;
    }
}
