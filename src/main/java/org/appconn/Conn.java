package org.appconn;

import org.appconn.channel.AbstractClient;
import org.appconn.mgmt.Address;

import java.io.IOException;

/**
 * <i>appconn</i> Conn.
 */
final class Conn extends AbstractConn {

    private AbstractClient client;

    Conn(int id, int mappingKey, String name, Address address, byte state, int connectTimeout) throws IOException {
        super(id, mappingKey, name, address, state, connectTimeout);
        client = AbstractClient.newTcpClient(address.toSocketAddress(), this.connectTimeout);
    }

    @Override
    AbstractClient getClient() {
        return client;
    }
}
