package org.appconn;

import org.appconn.channel.AbstractClient;
import org.appconn.mgmt.Address;

import java.io.IOException;

/**
 * <i>appconn</i> Connless.
 */
final class Connless extends AbstractConn {

    Connless(int id, int mappingKey, String name, Address address, byte state, int connectTimeout) {
        super(id, mappingKey, name, address, state, connectTimeout);
    }

    @Override
    AbstractClient getClient() {
        try {
            return AbstractClient.newSingletonReturnTcpClient(getAddress().toSocketAddress(), connectTimeout);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
