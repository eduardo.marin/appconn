package org.appconn;

/**
 * Interruptible interface.
 */
public interface Interruptible {

    /**
     * This method will be called when the AppServer state is set to {@code OFFLINE} or with
     * {@link org.appconn.Appconn} {@code clear()} method.
     */
    void interrupt();
}
