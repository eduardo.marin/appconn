package org.appconn;

import org.appconn.mgmt.StatsEntry;

import java.util.Collection;

/**
 * StatsHandler interface.
 */
public interface StatsHandler {

    void handle(int id, Collection<StatsEntry> stats);
}
