package org.appconn;

import java.util.Optional;

/**
 * <i>appconn</i> ConnGetter.
 */
@FunctionalInterface
public interface ConnGetter {

    /**
     * Gets the required AbstractConn.
     * @param serviceClient the ServiceClient
     * @param methodKey     the methodKey
     * @param args          the method args
     * @return the required AbstractConn for the application
     */
    Optional<AbstractConn> getConn(ServiceClient serviceClient, int methodKey, Object... args);

    /** {@code FIRST} connGetter. */
    ConnGetter FIRST = (serviceClient, methodKey, args) -> serviceClient.first();

    /** {@code NEXT} connGetter. */
    ConnGetter NEXT = (serviceClient, methodKey, args) -> serviceClient.next();
}
