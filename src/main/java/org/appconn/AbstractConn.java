package org.appconn;

import org.appconn.channel.AbstractClient;
import org.appconn.mgmt.Address;
import org.appconn.mgmt.Server;

/**
 * <i>appconn</i> AbstractConn.
 */
public abstract class AbstractConn extends Server {

    protected final int connectTimeout;

    AbstractConn(int id, int mappingKey, String name, Address address, byte state, int connectTimeout) {
        super(id, mappingKey, name, address, state);
        this.connectTimeout = connectTimeout;
    }

    abstract AbstractClient getClient();
}
