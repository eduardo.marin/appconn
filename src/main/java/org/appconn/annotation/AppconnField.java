package org.appconn.annotation;

import java.lang.annotation.*;

/**
 * Annotation used to indicate an {@link org.appconn.annotation.AppconnType AppconnType}
 * class field is intended for <i>appconn</i> serialization.
 * @since 1.8
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.FIELD})
public @interface AppconnField {
}
