package org.appconn.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * Annotation used to define an <i>appconn</i> service.
 *
 * <p>This annotation is intended to be used on {@code public} interfaces.
 * The {@code @AppconnService} annotated interface should include the definition of
 * {@link org.appconn.annotation.AppconnMethod AppconnMethod} annotated methods intended
 * for remote calling.
 *
 * <p>The {@code @AppconnService} annotated interface should be implemented and instanced
 * for publishing on the server side, or <em><i>appconn</i></em> instantiated on the client side.
 * Implementation is only required on the server, client only requires access to the
 * {@code @AppconnService} annotated interface.
 * @since 1.8
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
public @interface AppconnService {

    /**
     * @return the amount of units for the connectTimeout
     */
    int connectTimeout() default 4;

    /**
     * @return the TimeUnit used for the connectTimeout
     */
    TimeUnit unit() default TimeUnit.SECONDS;

    /**
     * @return the round trip delay in millis
     */
    int delayMillis() default 8;
}
