package org.appconn.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * Annotation used to indicate an {@link org.appconn.annotation.AppconnService AppconnService}
 * interface method is intended for <i>appconn</i> remote calling.
 * @since 1.8
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.METHOD})
public @interface AppconnMethod {

    /**
     * If a method adds statistics, the client will report the elapsed time in milliseconds to the
     * <i>appconn</i> Mgmt.
     * To handle this information, a {@link org.appconn.StatsHandler StatsHandler} implementation
     * must be set to the <i>appconn</i> local Mgmt.
     * The {@link org.appconn.mgmt.StatsEntry StatsEntry} objects contain the method key.
     * @return {@code true} if the method adds statistics
     */
    boolean addStats() default false;

    /**
     * The key of an {@code AppconnMethod} is serialized on each invocation, to improve
     * efficiency, keys can be defined manually to use small integers on frequently used
     * methods.
     * Keys from 8400 to 8499 are reserved for internal <i>appconn</i> use.
     * @return the key used for unique identification
     */
    int key() default 0;

    /**
     * If a method is timed, it will block until the return of the call is received or the
     * timeout is expired.
     * If a method is not timed, it will block until the return of the call is received.
     * @return {@code true} if the method has a timeout
     */
    boolean timed() default true;

    /**
     * @return the amount of units for the timeout
     */
    long timeout() default 32;

    /**
     * @return the TimeUnit used for the timeout
     */
    TimeUnit unit() default TimeUnit.SECONDS;
}
