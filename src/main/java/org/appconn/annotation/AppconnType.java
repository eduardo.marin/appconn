package org.appconn.annotation;

import java.lang.annotation.*;

/**
 * Annotation used to indicate an instance of the annotated class is intended
 * for <i>appconn</i> serialization.
 *
 * <p>Fields required to be serialized should include {@link org.appconn.annotation.AppconnField AppconnField}
 * annotation.
 *
 * <p>If an annotated field is declared as {@code private}, public setters and getters are required
 * and should be defined. To avoid the use of public setters, a public constructor with all
 * {@code @AppconnField} fields <em>in order</em> of declaration should be defined,
 * this should allow the use of {@code final} modifier if it is required.
 * @since 1.8
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {ElementType.TYPE})
public @interface AppconnType {
}
