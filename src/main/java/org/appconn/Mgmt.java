package org.appconn;

import org.appconn.mgmt.*;

import java.sql.*;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * <i>appconn</i> Mgmt service implementation.
 */
final class Mgmt implements MgmtService {

    private static final long MGMT_TIMER_PERIOD = 32_000L;

    private final Server server;
    private final Collection<Server> servers;
    private final Collection<Service> services;

    private final String url;
    private final String user;
    private final String password;

    private StatsHandler statsHandler;

    private final Timer mgmtTimer;

    private final Predicate<Server> isThis;

    Mgmt(Server server, SortedSet<Server> servers, SortedSet<Service> services, String url, String user, String password) {
        this.server = server;
        this.servers = servers;
        this.services = services;

        this.url = url;
        this.user = user;
        this.password = password;

        statsHandler = null;

        mgmtTimer = new Timer(getClass().getName());
        mgmtTimer.scheduleAtFixedRate(new MgmtTimerTask(), MGMT_TIMER_PERIOD, MGMT_TIMER_PERIOD);

        isThis = s -> s.getId() == server.getId();
    }

    @Override
    public synchronized void addServer(Server server) throws MgmtException {
        if (server == null) throw new MgmtException("null server");
        if (Server.IS_ONLINE.or(Server.IS_SPARE).negate().test(server))
            throw new MgmtException("unsupported state " + server.getState());
        if (!servers.add(server))
            throw new MgmtException("servers contains id " + server.getId());
        Logger.getLogger("org.appconn").log(Level.CONFIG, server + " added");

        getMgmtServiceServers().forEach(s -> {
            try {
                new MgmtReplServiceClient(s.getAddress().toSocketAddress()).addServerRepl(server);
            } catch (Exception e) {
                Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
            }
        });
    }

    @Override
    public synchronized void removeServer(int id) {
        getServer(id).ifPresent(server -> {
            setServerState(server, Server.STATE.OFFLINE.byteValue());
            Logger.getLogger("org.appconn").log(Level.CONFIG, server + " removed");

            getMgmtServiceServers().forEach(s -> {
                try {
                    new MgmtReplServiceClient(s.getAddress().toSocketAddress()).removeServerRepl(id);
                } catch (Exception e) {
                    Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                }
            });
        });
    }

    @Override
    public synchronized void publish(String serviceName, int id) throws MgmtException {
        Server server = getServer(id).orElseThrow(() -> new MgmtException("null server"));
        computeIfAbsentService(serviceName).getServers().add(server);
        if (Server.IS_ONLINE.test(server)) setServers(serviceName);
        Logger.getLogger("org.appconn").log(Level.CONFIG, serviceName + " id=" + id + " added");

        getMgmtServiceServers().forEach(s -> {
            try {
                new MgmtReplServiceClient(s.getAddress().toSocketAddress()).publishRepl(serviceName, id);
            } catch (Exception e) {
                Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
            }
        });
    }

    @Override
    public synchronized void addClient(String serviceName, Client client) {
        computeIfAbsentService(serviceName).getClients().add(client);
        Logger.getLogger("org.appconn").log(Level.CONFIG, serviceName + " " + client + " added");

        getMgmtServiceServers().forEach(s -> {
            try {
                new MgmtReplServiceClient(s.getAddress().toSocketAddress()).addClientRepl(serviceName, client);
            } catch (Exception e) {
                Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
            }
        });
    }

    @Override
    public synchronized void removeClient(String serviceName, Client client) {
        getService(serviceName).ifPresent(service -> {
            service.getClients().remove(client);
            if (service.isEmpty()) services.remove(service);
            Logger.getLogger("org.appconn").log(Level.CONFIG, serviceName + " " + client + " removed");

            getMgmtServiceServers().forEach(s -> {
                try {
                    new MgmtReplServiceClient(s.getAddress().toSocketAddress()).removeClientRepl(serviceName, client);
                } catch (Exception e) {
                    Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                }
            });
        });
    }

    @Override
    public void addStats(int id, Collection<StatsEntry> stats) {
        Optional.ofNullable(statsHandler).ifPresent(s -> s.handle(id, stats));
    }

    @Override
    public ServerProperties getServerProperties(int id) throws MgmtException {
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            PreparedStatement ps = connection.prepareStatement(SERVER_SELECT);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return new ServerProperties(PARSE_INTEGER.apply(rs.getString(SERVER.BACKLOG.getName())),
                                            PARSE_INTEGER.apply(rs.getString(SERVER.SELECTORS_LENGTH.getName())),
                                            PARSE_INTEGER.apply(rs.getString(SERVER.POOL_SIZE.getName())),
                                            PARSE_INTEGER.apply(rs.getString(SERVER.QUEUE_CAPACITY.getName())),
                                            id,
                                            rs.getInt(SERVER.MAPPING_KEY.getName()),
                                            rs.getString(SERVER.NAME.getName()),
                                            new Address(rs.getString(SERVER.ADDRESS.getName())),
                                            rs.getByte(SERVER.STATE.getName()));
            } else throw new MgmtException("null serverProperties id=" + id);
        } catch (IllegalArgumentException | SQLException e) {
            throw new MgmtException(e.getMessage(), e);
        }
    }

    @Override
    public Properties getProperties(int id) throws MgmtException {
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            Properties properties = new Properties();
            PreparedStatement ps = connection.prepareStatement(PROPERTIES_SELECT);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) properties.setProperty(rs.getString(PROPERTIES.NAME.getName()), rs.getString(PROPERTIES.VALUE.getName()));
            return properties;
        } catch (SQLException e) {
            throw new MgmtException(e.getMessage(), e);
        }
    }

    @Override
    public synchronized void setServerState(int id, byte state) {
        getServer(id).ifPresent(server -> {
            if (server.getState() != state &&
                (Server.IS_OFFLINE.or(Server.IS_ONLINE).or(Server.IS_SPARE).test(server))) {
                setServerState(server, state);
                Logger.getLogger("org.appconn").log(Level.CONFIG, server.toString());

                getMgmtServiceServers().forEach(s -> {
                    try {
                        new MgmtReplServiceClient(s.getAddress().toSocketAddress()).setServerStateRepl(id, state);
                    } catch (Exception e) {
                        Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                    }
                });

                try {
                    if (isThis.negate().test(server))
                        new AppServerServiceClient(server.getAddress().toSocketAddress()).setState(state);
                } catch (Exception e) {
                    Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                }
            }
        });
    }

    @Override
    public synchronized Collection<Server> getServers(String serviceName) {
        return getService(serviceName).map(Service::getServers)
                                      .orElse(Collections.emptySet())
                                      .stream()
                                      .filter(Server.IS_ONLINE)
                                      .collect(Collectors.toCollection(TreeSet::new));
    }

    @Override
    public synchronized Collection<Server> getServers() {
        return new TreeSet<>(servers);
    }

    @Override
    public synchronized Collection<Service> getServices() {
        return services.stream()
                       .map(service -> new Service(service.getServiceName(), service.getServers(), service.getClients()))
                       .collect(Collectors.toCollection(TreeSet::new));
    }

    @Override
    public synchronized void addServerRepl(Server server) {
        if (servers.add(server)) Logger.getLogger("org.appconn").log(Level.CONFIG, server + " added");
    }

    @Override
    public synchronized void removeServerRepl(int id) {
        getServer(id).ifPresent(server -> {
            setServerStateRepl(server, Server.STATE.OFFLINE.byteValue());
            Logger.getLogger("org.appconn").log(Level.CONFIG, server + " removed");
        });
    }

    @Override
    public synchronized void publishRepl(String serviceName, int id) {
        getServer(id).ifPresent(s -> {
            computeIfAbsentService(serviceName).getServers().add(s);
            Logger.getLogger("org.appconn").log(Level.CONFIG, serviceName + " id=" + id + " added");
        });
    }

    @Override
    public synchronized void addClientRepl(String serviceName, Client client) {
        computeIfAbsentService(serviceName).getClients().add(client);
        Logger.getLogger("org.appconn").log(Level.CONFIG, serviceName + " " + client + " added");
    }

    @Override
    public synchronized void removeClientRepl(String serviceName, Client client) {
        getService(serviceName).ifPresent(service -> {
            service.getClients().remove(client);
            if (service.isEmpty()) services.remove(service);
            Logger.getLogger("org.appconn").log(Level.CONFIG, serviceName + " " + client + " removed");
        });
    }

    @Override
    public synchronized void setServerStateRepl(int id, byte state) {
        getServer(id).ifPresent(server -> {
            setServerStateRepl(server, state);
            Logger.getLogger("org.appconn").log(Level.CONFIG, server.toString());
        });
    }

    void setStatsHandler(StatsHandler statsHandler) {
        this.statsHandler = statsHandler;
    }

    void clear() {
        mgmtTimer.cancel();
    }

    private void setServerState(Server server, byte state) {
        List<String> serviceNames = new ArrayList<>();
        // Server.OFFLINE
        if (Server.STATE.IS_OFFLINE.test(state)) {
            services.removeIf(service -> {
                if (service.getServers().remove(server)) {
                    serviceNames.add(service.getServiceName());
                    return service.isEmpty();
                }
                return false;
            });
            servers.remove(server);
        }
        // Server.ONLINE || Server.SPARE
        else {
            services.forEach(service -> {
                if (service.getServers().contains(server)) serviceNames.add(service.getServiceName());
            });
        }
        server.setState(state);
        serviceNames.forEach(this::setServers);

        // Server.SPARE to Server.ONLINE
        if (Server.STATE.IS_OFFLINE.test(state)) {
            Collection<Server> serversByMappingKey = getServers(server.getMappingKey());
            if (serversByMappingKey.stream().noneMatch(Server.IS_ONLINE)) {
                serversByMappingKey.stream()
                                   .filter(Server.IS_SPARE)
                                   .findFirst()
                                   .ifPresent(s -> setServerState(s.getId(), Server.STATE.ONLINE.byteValue()));
            }
        }
    }

    private void setServerStateRepl(Server server, byte state) {
        if (Server.STATE.IS_OFFLINE.test(state)) {
            services.removeIf(service -> service.getServers().remove(server) && service.isEmpty());
            servers.remove(server);
        }
        server.setState(state);
    }

    private void setServers(String serviceName) {
        getService(serviceName).map(Service::getClients)
                               .orElse(Collections.emptySet())
                               .removeIf(client -> {
                                   try {
                                       new AppClientServiceClient(client.getAddress().toSocketAddress()).setServers(serviceName, getServers(serviceName));
                                       return false;
                                   } catch (Exception e) {
                                       Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                                       return true;
                                   }
                               });
    }

    private Optional<Server> getServer(int id) {
        return servers.stream()
                      .filter(server -> server.getId() == id)
                      .findFirst();
    }

    private Collection<Server> getServers(int mappingKey) {
        return servers.stream()
                      .filter(server -> server.getMappingKey() == mappingKey)
                      .collect(Collectors.toCollection(TreeSet::new));
    }

    private Optional<Service> getService(String serviceName) {
        return services.stream()
                       .filter(service -> service.getServiceName().equals(serviceName))
                       .findFirst();
    }

    private Service computeIfAbsentService(String serviceName) {
        return getService(serviceName).orElseGet(() -> {
            Service service = new Service(serviceName);
            services.add(service);
            return service;
        });
    }

    private Collection<Server> getMgmtServiceServers() {
        return getService(MgmtService.class.getName()).map(Service::getServers)
                                                      .orElse(Collections.emptySet())
                                                      .stream()
                                                      .filter(isThis.negate())
                                                      .collect(Collectors.toCollection(TreeSet::new));
    }

    private class MgmtTimerTask extends TimerTask {

        @Override
        public void run() {
            Collection<Server> tmpServers;
            synchronized (this) {
                if (Server.IS_ONLINE.test(server))
                    tmpServers = servers.stream()
                                        .filter(isThis.negate())
                                        .collect(Collectors.toCollection(TreeSet::new));
                else if (Server.IS_SPARE.test(server)) tmpServers = getMgmtServiceServers();
                else tmpServers = Collections.emptySet();
            }
            tmpServers.forEach(server -> {
                try {
                    byte state = new AppServerServiceClient(server.getAddress().toSocketAddress()).getState();
                    if (server.getState() != state) setServerState(server.getId(), state);
                } catch (Exception e) {
                    Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                    setServerState(server.getId(), Server.STATE.OFFLINE.byteValue());
                }
            });
        }
    }

    private static final Function<String, Integer> PARSE_INTEGER = s -> s != null ? Integer.parseInt(s) : null;

    private static final String SERVER_ENTITY = "server";

    /**
     * The SERVER enum.
     */
    private enum SERVER {

        ID                  ("id"),
        MAPPING_KEY         ("mapping_key"),
        NAME                ("name"),
        ADDRESS             ("address"),
        STATE               ("state"),
        BACKLOG             ("backlog"),
        SELECTORS_LENGTH    ("selectors_length"),
        POOL_SIZE           ("pool_size"),
        QUEUE_CAPACITY      ("queue_capacity");

        private final String name;

        SERVER(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static final String SERVER_SELECT = "SELECT " +
                                                SERVER.MAPPING_KEY.getName() + ',' +
                                                SERVER.NAME.getName() + ',' +
                                                SERVER.ADDRESS.getName() + ',' +
                                                SERVER.STATE.getName() + ',' +
                                                SERVER.BACKLOG.getName() + ',' +
                                                SERVER.SELECTORS_LENGTH.getName() + ',' +
                                                SERVER.POOL_SIZE.getName() + ',' +
                                                SERVER.QUEUE_CAPACITY.getName() +
                                                " FROM " + SERVER_ENTITY +
                                                " WHERE " + SERVER.ID.getName() + "=?";

    private static final String PROPERTIES_ENTITY = "properties";

    /**
     * The PROPERTIES enum.
     */
    private enum PROPERTIES {

        ID      ("id"),
        NAME    ("name"),
        VALUE   ("value");

        private final String name;

        PROPERTIES(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private static final String PROPERTIES_SELECT = "SELECT " +
                                                    PROPERTIES.NAME.getName() + ',' +
                                                    PROPERTIES.VALUE.getName() +
                                                    " FROM " + PROPERTIES_ENTITY +
                                                    " WHERE " + PROPERTIES.ID.getName() + "=?";
}
