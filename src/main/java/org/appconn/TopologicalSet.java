package org.appconn;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Topological set that implements deep first search.
 */
final class TopologicalSet {

    private final Map<Class<?>, Tmp> tmpMap;
    private final Deque<Class<?>> reversePost;
    private Deque<Class<?>> cycle;

    TopologicalSet(Class<?> service) throws CoderException {
        tmpMap = new HashMap<>();
        reversePost = new ArrayDeque<>();

        for (Method m : service.getMethods()) {
            if (Coders.IS_APPCONN_METHOD.test(m)) {
                put(Coders.topologicalType(m.getReturnType(), m.getGenericReturnType()));
                for (Parameter p : m.getParameters())
                    put(Coders.topologicalType(p.getType(), p.getParameterizedType()));
            }
        }
    }

    private Tmp getTmp(Class<?> type) {
        return tmpMap.computeIfAbsent(type, k -> new Tmp());
    }

    private void dfs(Class<?> v) throws CoderException {
        Tmp vTmp = getTmp(v);
        vTmp.marked = true;
        vTmp.onStack = true;

        List<Field> appconnFields = Coders.appconnFields(v);
        if (appconnFields.isEmpty())
            throw new CoderException(v.getName() + " has empty AppconnField list");
        for (Field f : appconnFields) {
            Class<?> w = Coders.topologicalType(f.getType(), f.getGenericType());

            if (Coders.IS_APPCONN_TYPE.test(w)) {
                if (Coders.IS_ABSTRACT.test(w)) throw new CoderException(w.getName() + " is abstract");
                if (cycle != null) return;

                Tmp wTmp = getTmp(w);
                if (!wTmp.marked) {
                    wTmp.edgeTo = v;
                    dfs(w);
                } else if (wTmp.onStack) {
                    cycle = new ArrayDeque<>();
                    for (Class<?> x = v; !x.equals(w); x = getTmp(x).edgeTo) cycle.push(x);
                    cycle.push(w);
                    cycle.push(v);
                }
            }
        }
        vTmp.onStack = false;
        if (!reversePost.contains(v)) reversePost.add(v);
    }

    private void put(Class<?> v) throws CoderException {
        if (Coders.IS_APPCONN_TYPE.test(v)) {
            if (Coders.IS_ABSTRACT.test(v)) throw new CoderException(v.getName() + " is abstract");
            else if (!tmpMap.containsKey(v)) dfs(v);
        }
    }

    Collection<Class<?>> getSortedAppconnTypes() {
        return reversePost;
    }

    boolean hasCycle() {
        return cycle != null;
    }

    Collection<Class<?>> getCycle() {
        return cycle;
    }

    String getCycleString() {
        return cycle != null ? cycle.stream().map(Class::getName).collect(Collectors.joining(" \u2192 ")) : "";
    }

    private class Tmp {

        private boolean marked = false;
        private boolean onStack = false;
        private Class<?> edgeTo;
    }
}
