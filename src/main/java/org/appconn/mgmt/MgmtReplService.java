package org.appconn.mgmt;

import org.appconn.annotation.AppconnMethod;
import org.appconn.annotation.AppconnService;

/**
 * For <i>appconn</i> internal use.
 */
@AppconnService
public interface MgmtReplService {

    @AppconnMethod(key = 8420)
    void addServerRepl(Server server);

    @AppconnMethod(key = 8421)
    void removeServerRepl(int id);

    @AppconnMethod(key = 8422)
    void publishRepl(String serviceName, int id);

    @AppconnMethod(key = 8423)
    void addClientRepl(String serviceName, Client client);

    @AppconnMethod(key = 8424)
    void removeClientRepl(String serviceName, Client client);

    @AppconnMethod(key = 8425)
    void setServerStateRepl(int id, byte state);
}
