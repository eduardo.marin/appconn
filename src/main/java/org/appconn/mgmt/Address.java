package org.appconn.mgmt;

import org.appconn.annotation.AppconnField;
import org.appconn.annotation.AppconnType;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.URI;

/**
 * <i>appconn</i> Address.
 */
@AppconnType
public class Address implements Comparable<Address> {

    private static final String SCHEME = "appconn";
    private static final int DEFAULT_PORT = 8482;

    @AppconnField
    private final String host;
    @AppconnField
    private final int port;

    /**
     * Constructs a new Address.
     * @param host the host
     * @param port the port number
     */
    public Address(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * Constructs a new Address from URI formatted string <em>appconn://<i>host</i>:<i>port</i></em> where
     * port can be omitted when the default port is used (port number 8482).
     * @param address the URI formatted string
     * @throws IllegalArgumentException if a different scheme or URI without host
     */
    public Address(String address) {
        URI uri = URI.create(address);
        if (!uri.getScheme().equals(SCHEME)) throw new IllegalArgumentException("illegal scheme");
        if (uri.getHost() == null) throw new IllegalArgumentException("null host");
        host = uri.getHost();
        port = uri.getPort() != -1 ? uri.getPort() : DEFAULT_PORT;
    }

    @Override
    public String toString() {
        return SCHEME + "://" + host + (port != DEFAULT_PORT ? ":" + port : "");
    }

    @Override
    public int compareTo(Address o) {
        int compareTo = host.compareTo(o.getHost());
        return compareTo != 0 ? compareTo : Integer.compare(port, o.getPort());
    }

    /**
     * Returns a new SocketAddress constructed with host and port fields.
     * @return the SocketAddress
     */
    public SocketAddress toSocketAddress() {
        return host != null ? new InetSocketAddress(host, port) : new InetSocketAddress(port);
    }

    /**
     * @return the host
     */
    public String getHost() {
        return host;
    }

    /**
     * @return the port number
     */
    public int getPort() {
        return port;
    }
}
