package org.appconn.mgmt;

import org.appconn.annotation.AppconnField;
import org.appconn.annotation.AppconnType;

/**
 * <i>appconn</i> StatsEntry.
 */
@AppconnType
public class StatsEntry {

    @AppconnField
    private final int key;
    @AppconnField
    private int n;
    @AppconnField
    private long millisSum;
    @AppconnField
    private long millisSquaredSum;

    /**
     * Constructs a new StatsEntry.
     * @param key              the stats key
     * @param n                the number of occurrences
     * @param millisSum        the milliseconds sum
     * @param millisSquaredSum the milliseconds squared sum
     */
    public StatsEntry(int key, int n, long millisSum, long millisSquaredSum) {
        this.key = key;
        this.n = n;
        this.millisSum = millisSum;
        this.millisSquaredSum = millisSquaredSum;
    }

    /**
     * Adds the given n, millisSum and millisSquaredSum.
     */
    public StatsEntry add(int n, long millisSum, long millisSquaredSum) {
        this.n += n;
        this.millisSum += millisSum;
        this.millisSquaredSum += millisSquaredSum;
        return this;
    }

    /**
     * @return the key
     */
    public int getKey() {
        return key;
    }

    /**
     * @return the number of occurrences
     */
    public int getN() {
        return n;
    }

    /**
     * @return the milliseconds sum
     */
    public long getMillisSum() {
        return millisSum;
    }

    /**
     * @return the milliseconds squared sum
     */
    public long getMillisSquaredSum() {
        return millisSquaredSum;
    }
}
