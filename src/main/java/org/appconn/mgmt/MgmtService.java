package org.appconn.mgmt;

import org.appconn.annotation.AppconnMethod;
import org.appconn.annotation.AppconnService;

import java.util.Collection;

/**
 * For <i>appconn</i> internal use.
 */
@AppconnService
public interface MgmtService extends MgmtOpsService, MgmtReplService {

    @AppconnMethod(key = 8400)
    void addServer(Server server) throws MgmtException;

    @AppconnMethod(key = 8401)
    void removeServer(int id);

    @AppconnMethod(key = 8402)
    void publish(String serviceName, int id) throws MgmtException;

    @AppconnMethod(key = 8403)
    void addClient(String serviceName, Client client);

    @AppconnMethod(key = 8404)
    void removeClient(String serviceName, Client client);

    @AppconnMethod(key = 8405)
    void addStats(int id, Collection<StatsEntry> stats);
}
