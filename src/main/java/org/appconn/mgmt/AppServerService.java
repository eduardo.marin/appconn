package org.appconn.mgmt;

import org.appconn.annotation.AppconnMethod;
import org.appconn.annotation.AppconnService;

/**
 * For <i>appconn</i> internal use.
 */
@AppconnService
public interface AppServerService {

    @AppconnMethod(key = 8430)
    byte getState();

    @AppconnMethod(key = 8431)
    void setState(byte state);
}
