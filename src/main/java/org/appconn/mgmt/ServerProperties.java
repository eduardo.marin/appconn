package org.appconn.mgmt;

import org.appconn.annotation.AppconnField;
import org.appconn.annotation.AppconnType;

/**
 * <i>appconn</i> ServerProperties.
 */
@AppconnType
public class ServerProperties extends Server {

    @AppconnField
    private final Integer backlog;
    @AppconnField
    private final Integer selectorsLength;
    @AppconnField
    private final Integer poolSize;
    @AppconnField
    private final Integer queueCapacity;

    /**
     * Constructs a new ServerProperties.
     * @param backlog         the server backlog
     * @param selectorsLength the number of server selectors
     * @param poolSize        the poolSize for ThreadPoolExecutor
     * @param queueCapacity   the queueCapacity for pool waiting queue
     * @param id              the server id
     * @param mappingKey      the server mappingKey
     * @param name            the server name
     * @param address         the server Address
     * @param state           the server state
     */
    public ServerProperties(Integer backlog, Integer selectorsLength, Integer poolSize, Integer queueCapacity, int id, int mappingKey, String name, Address address, byte state) {
        super(id, mappingKey, name, address, state);
        this.backlog = backlog;
        this.selectorsLength = selectorsLength;
        this.poolSize = poolSize;
        this.queueCapacity = queueCapacity;
    }

    /**
     * Constructs a new ServerProperties.
     * @param id         the server id
     * @param mappingKey the server mappingKey
     * @param name       the server name
     * @param address    the server Address
     * @param state      the server state
     */
    public ServerProperties(int id, int mappingKey, String name, Address address, byte state) {
        this(null, null, null, null, id, mappingKey, name, address, state);
    }

    /**
     * @return the server backlog
     */
    public Integer getBacklog() {
        return backlog;
    }

    /**
     * @return the number of server selectors
     */
    public Integer getSelectorsLength() {
        return selectorsLength;
    }

    /**
     * @return the poolSize for ThreadPoolExecutor
     */
    public Integer getPoolSize() {
        return poolSize;
    }

    /**
     * @return the queueCapacity for pool waiting queue
     */
    public Integer getQueueCapacity() {
        return queueCapacity;
    }
}
