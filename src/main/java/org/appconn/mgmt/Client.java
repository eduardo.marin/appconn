package org.appconn.mgmt;

import org.appconn.annotation.AppconnField;
import org.appconn.annotation.AppconnType;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * <i>appconn</i> Client.
 */
@AppconnType
public class Client implements Comparable<Client> {

    @AppconnField
    private final Address address;
    @AppconnField
    private final byte connsType;

    public Client(Address address, byte connsType) {
        this.address = address;
        this.connsType = connsType;
    }

    @Override
    public String toString() {
        return "address=" + address + " connsType=" + CONNS_TYPE.valueOf(connsType);
    }

    @Override
    public int compareTo(Client o) {
        int compareTo = address.compareTo(o.getAddress());
        return compareTo != 0 ? compareTo : Byte.compare(connsType, o.getConnsType());
    }

    /**
     * @return the Address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Gets the connsType, {@code 0} if {@code CONN} or {@code 1} if {@code CONNLESS}.
     * @return the CONNS_TYPE byteValue
     */
    public byte getConnsType() {
        return connsType;
    }

    /**
     * The CONNS_TYPE enum.
     */
    public enum CONNS_TYPE {

        CONN        ((byte) 0),
        CONNLESS    ((byte) 1);

        private final byte byteValue;

        CONNS_TYPE(byte byteValue) {
            this.byteValue = byteValue;
        }

        public byte byteValue() {
            return byteValue;
        }

        public static Optional<CONNS_TYPE> valueOf(byte byteValue) {
            return Stream.of(CONNS_TYPE.values()).filter(connsType -> connsType.byteValue == byteValue).findFirst();
        }
    }
}
