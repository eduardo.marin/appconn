package org.appconn.mgmt;

import org.appconn.annotation.AppconnField;
import org.appconn.annotation.AppconnType;

import java.util.Collection;
import java.util.TreeSet;

/**
 * <i>appconn</i> Service.
 */
@AppconnType
public class Service implements Comparable<Service> {

    @AppconnField
    private final String serviceName;
    @AppconnField
    private final Collection<Server> servers;
    @AppconnField
    private final Collection<Client> clients;

    public Service(String serviceName, Collection<Server> servers, Collection<Client> clients) {
        this.serviceName = serviceName;
        this.servers = new TreeSet<>(servers);
        this.clients = new TreeSet<>(clients);
    }

    public Service(String serviceName) {
        this.serviceName = serviceName;
        servers = new TreeSet<>();
        clients = new TreeSet<>();
    }

    @Override
    public int compareTo(Service o) {
        return serviceName.compareTo(o.getServiceName());
    }

    /**
     * @return the serviceName
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * @return true if the service is empty
     */
    public boolean isEmpty() {
        return servers.isEmpty() && clients.isEmpty();
    }

    /**
     * @return the Collection of servers
     */
    public Collection<Server> getServers() {
        return servers;
    }

    /**
     * @return the Collection of clients
     */
    public Collection<Client> getClients() {
        return clients;
    }
}
