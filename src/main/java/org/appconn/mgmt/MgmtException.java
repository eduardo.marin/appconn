package org.appconn.mgmt;

import org.appconn.AppconnException;

/**
 * Thrown at <i>appconn</i> Mgmt services.
 */
public class MgmtException extends AppconnException {

    public MgmtException(String message) {
        super(message);
    }

    public MgmtException(String message, Throwable cause) {
        super(message, cause);
    }
}
