package org.appconn.mgmt;

import org.appconn.annotation.AppconnMethod;
import org.appconn.annotation.AppconnService;

import java.util.Collection;

/**
 * For <i>appconn</i> internal use.
 */
@AppconnService
public interface AppClientService {

    @AppconnMethod(key = 8432)
    void setServers(String serviceName, Collection<Server> servers);
}
