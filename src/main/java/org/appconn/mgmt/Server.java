package org.appconn.mgmt;

import org.appconn.annotation.AppconnField;
import org.appconn.annotation.AppconnType;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Stream;

/**
 * <i>appconn</i> Server.
 * <p>Server can be on one of three different states: {@code OFFLINE}, {@code ONLINE} or {@code SPARE},
 * the byte equivalent values are {@code 0}, {@code 1} or {@code 2} respectively.
 */
@AppconnType
public class Server implements Comparable<Server> {

    /** The IS_OFFLINE Predicate. */
    public static final Predicate<Server> IS_OFFLINE = s -> s.getState() == STATE.OFFLINE.byteValue();
    /** The IS_ONLINE Predicate. */
    public static final Predicate<Server> IS_ONLINE = s -> s.getState() == STATE.ONLINE.byteValue();
    /** The IS_SPARE Predicate. */
    public static final Predicate<Server> IS_SPARE = s -> s.getState() == STATE.SPARE.byteValue();

    @AppconnField
    private final int id;
    @AppconnField
    private final int mappingKey;
    @AppconnField
    private final String name;
    @AppconnField
    private final Address address;

    @AppconnField
    private volatile byte state;

    private LocalDateTime dateTime;

    public Server(int id, int mappingKey, String name, Address address, byte state) {
        this.id = id;
        this.mappingKey = mappingKey;
        this.name = name;
        this.address = address;
        this.state = state;
    }

    @Override
    public String toString() {
        return "id=" + id + " mappingKey=" + mappingKey + " name=" + name + " address=" + address + " state=" + STATE.valueOf(state).map(STATE::name).orElse(null);
    }

    @Override
    public int compareTo(Server o) {
        return Integer.compare(id, o.getId());
    }

    /**
     * @return the server id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the server mappingKey
     */
    public int getMappingKey() {
        return mappingKey;
    }

    /**
     * @return the server name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the Address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @return the server state
     */
    public byte getState() {
        return state;
    }

    /**
     * Sets the server state.
     * @param state the new state
     */
    public void setState(byte state) {
        this.state = state;
    }

    public Optional<LocalDateTime> getDateTime() {
        return Optional.ofNullable(dateTime);
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * The STATE enum.
     */
    public enum STATE {

        OFFLINE ((byte) 0),
        ONLINE  ((byte) 1),
        SPARE   ((byte) 2);

        private final byte byteValue;

        public static final Predicate<Byte> IS_OFFLINE = b -> b == OFFLINE.byteValue();
        public static final Predicate<Byte> IS_ONLINE = b -> b == ONLINE.byteValue();
        public static final Predicate<Byte> IS_SPARE = b -> b == SPARE.byteValue();

        STATE(byte byteValue) {
            this.byteValue = byteValue;
        }

        public byte byteValue() {
            return byteValue;
        }

        public static Optional<STATE> valueOf(byte byteValue) {
            return Stream.of(STATE.values()).filter(s -> s.byteValue == byteValue).findFirst();
        }
    }
}
