package org.appconn.mgmt;

import org.appconn.annotation.AppconnMethod;
import org.appconn.annotation.AppconnService;

import java.util.Collection;
import java.util.Properties;

/**
 * <i>appconn</i> Mgmt operations service.
 */
@AppconnService
public interface MgmtOpsService {

    /**
     * Gets the ServerProperties for the given id.
     * @param id the server id
     * @return the ServerProperties
     * @throws MgmtException if server properties are {@code null} or cannot be accessed
     */
    @AppconnMethod(key = 8410)
    ServerProperties getServerProperties(int id) throws MgmtException;

    /**
     * Gets the Properties for the given id.
     * @param id the server id
     * @return the Properties
     */
    @AppconnMethod(key = 8411)
    Properties getProperties(int id) throws MgmtException;

    /**
     * Sets the server state for the given id.
     * @param id    the server id
     * @param state the new state, state documentation at {@link org.appconn.mgmt.Server Server}
     */
    @AppconnMethod(key = 8412)
    void setServerState(int id, byte state);

    /**
     * Gets the Collection of {@code ONLINE} servers for the given {@code serviceName}.
     * @param serviceName the service name
     * @return the Collection of {@code ONLINE} servers
     */
    @AppconnMethod(key = 8413)
    Collection<Server> getServers(String serviceName);

    /**
     * @return the Collection of servers
     */
    @AppconnMethod(key = 8414)
    Collection<Server> getServers();

    /**
     * @return the Collection of services
     */
    @AppconnMethod(key = 8415)
    Collection<Service> getServices();
}
