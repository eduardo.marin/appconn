package org.appconn;

/**
 * Thrown when <i>appconn</i> requirements are not satisfied in terms of compiling auto-generated code.
 */
public class CoderException extends AppconnException {

    CoderException(String message) {
        super(message);
    }

    CoderException(String message, Throwable cause) {
        super(message, cause);
    }
}
