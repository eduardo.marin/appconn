package org.appconn;

import org.appconn.channel.AbstractServer;
import org.appconn.channel.TcpServer;
import org.appconn.mgmt.Address;
import org.appconn.mgmt.AppServerService;
import org.appconn.mgmt.Server;
import org.appconn.mgmt.ServerProperties;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <i>appconn</i> AppServer.
 */
final class AppServer extends Server implements AppServerService {

    private final Integer backlog;
    private final Integer selectorsLength;
    private final Integer poolSize;
    private final Integer queueCapacity;
    private AbstractServer server;

    private Interruptible interruptible;

    private AppServer(int id, int mappingKey, String name, Address address, byte state, Integer backlog, Integer selectorsLength, Integer poolSize, Integer queueCapacity) throws IOException {
        super(id, mappingKey, name, address, STATE.OFFLINE.byteValue());
        this.backlog = backlog;
        this.selectorsLength = selectorsLength;
        this.poolSize = poolSize;
        this.queueCapacity = queueCapacity;
        server = null;
        interruptible = null;
        setServerState(state);
    }

    AppServer(ServerProperties serverProperties) throws IOException {
        this(serverProperties.getId(),
             serverProperties.getMappingKey(),
             serverProperties.getName(),
             serverProperties.getAddress(),
             serverProperties.getState(),
             serverProperties.getBacklog(),
             serverProperties.getSelectorsLength(),
             serverProperties.getPoolSize(),
             serverProperties.getQueueCapacity());
    }

    @Override
    public synchronized byte getState() {
        return super.getState();
    }

    @Override
    public synchronized void setState(byte state) {
        try {
            setServerState(state);
        } catch (IOException e) {
            Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
        }
    }

    private void setServerState(byte state) throws IOException {
        if (super.getState() != state) {
            if (STATE.IS_ONLINE.or(STATE.IS_SPARE).test(state)) {
                if (server == null) {
                    server = new TcpServer(getAddress().toSocketAddress(),
                                           backlog != null ? backlog : TcpServer.DEFAULT_BACKLOG,
                                           selectorsLength != null ? selectorsLength : 4,
                                           poolSize != null ? poolSize : TcpServer.DEFAULT_POOL_SIZE,
                                           queueCapacity != null ? queueCapacity : TcpServer.DEFAULT_QUEUE_CAPACITY);
                }
                super.setState(state);
            } else if (STATE.IS_OFFLINE.test(state)) {
                if (server != null) {
                    server.interrupt();
                    server = null;
                }
                super.setState(state);
                if (interruptible != null) interruptible.interrupt();
            }
            Logger.getLogger("org.appconn").log(Level.CONFIG, super.toString());
        }
    }

    /**
     * @return the AbstractServer
     */
    AbstractServer getServer() {
        return server;
    }

    /**
     * Sets the Interruptible implementation.
     */
    void setInterruptible(Interruptible interruptible) {
        this.interruptible = interruptible;
    }
}
