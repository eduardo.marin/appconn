package org.appconn;

/**
 * Thrown when <i>appconn</i> requirements are not satisfied.
 */
public class AppconnException extends Exception {

    public AppconnException(String message) {
        super(message);
    }

    public AppconnException(String message, Throwable cause) {
        super(message, cause);
    }
}
