package org.appconn;

import org.appconn.mgmt.AppClientService;
import org.appconn.mgmt.Server;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * <i>appconn</i> AppClient.
 */
final class AppClient implements AppClientService {

    private final Map<Integer, Conn> connMap;
    private final Map<String, ServiceClient> serviceClientMap;
    private final Lock lock;

    AppClient() {
        connMap = new HashMap<>();
        serviceClientMap = new HashMap<>();
        lock = new ReentrantLock();
    }

    /**
     * Sets the servers for the given {@code serviceName} and Collection of servers.
     */
    @Override
    public void setServers(String serviceName, Collection<Server> servers) {
        lock.lock();
        try {
            ServiceClient serviceClient = serviceClientMap.get(serviceName);
            if (serviceClient != null) {
                switch (serviceClient.getConnsType()) {
                    case CONN:
                        SortedSet<AbstractConn> conns = new TreeSet<>();
                        // add
                        servers.forEach(server -> {
                            AbstractConn conn = connMap.computeIfAbsent(server.getId(), k -> {
                                try {
                                    return new Conn(server.getId(),
                                                    server.getMappingKey(),
                                                    server.getName(),
                                                    server.getAddress(),
                                                    server.getState(),
                                                    serviceClient.getConnectTimeout());
                                } catch (IOException e) {
                                    Logger.getLogger("org.appconn").log(Level.WARNING, "conn to " + server.getAddress() + " failed", e);
                                    return null;
                                }
                            });
                            if (conn != null) conns.add(conn);
                        });
                        Set<AbstractConn> removeConns = serviceClient.getConns()
                                                                     .stream()
                                                                     .filter(conn -> !servers.contains(conn))
                                                                     .collect(Collectors.toSet());

                        serviceClient.setConns(conns);
                        // remove
                        removeConns.forEach(conn -> {
                            if (serviceClientMap.values()
                                                .stream()
                                                .filter(ServiceClient.IS_CONN)
                                                .noneMatch(tmpServiceClient -> tmpServiceClient.getConns()
                                                                                               .stream()
                                                                                               .anyMatch(tmpConn -> tmpConn.getId() == conn.getId()))) {
                                connMap.remove(conn.getId());
                                conn.getClient().interrupt();
                            }
                        });
                        break;
                    case CONNLESS:
                        serviceClient.setConns(servers.stream()
                                                      .map(server -> new Connless(server.getId(),
                                                                                  server.getMappingKey(),
                                                                                  server.getName(),
                                                                                  server.getAddress(),
                                                                                  server.getState(),
                                                                                  serviceClient.getConnectTimeout()))
                                                      .collect(Collectors.toCollection(TreeSet::new)));
                        break;
                }
                Logger.getLogger("org.appconn").log(Level.CONFIG, serviceName + " " + servers);
            }
        } finally {
            lock.unlock();
        }
    }

    @SuppressWarnings("unchecked")
    <T> Optional<T> getServiceClient(Class<T> service) {
        lock.lock();
        try {
            return Optional.ofNullable((T) serviceClientMap.get(service.getName()));
        } finally {
            lock.unlock();
        }
    }

    /**
     * Gets the ServiceClient for the given {@code serviceName}.
     * @return the ServiceClient
     */
    ServiceClient getServiceClient(String serviceName) {
        return serviceClientMap.get(serviceName);
    }

    /**
     * Puts the ServiceClient for the given {@code serviceName}.
     */
    void putServiceClient(String serviceName, ServiceClient serviceClient) {
        serviceClientMap.put(serviceName, serviceClient);
    }

    /**
     * @return the Collection of serviceNames
     */
    Collection<String> getServiceNames() {
        return serviceClientMap.keySet();
    }

    /**
     * Clears the AppClient.
     */
    void clear() {
        connMap.values().forEach(conn -> conn.getClient().interrupt());
        connMap.clear();
        serviceClientMap.clear();
    }

    /**
     * Locks the AppClient.
     */
    void lock() {
        lock.lock();
    }

    /**
     * Unlocks the AppClient.
     */
    void unlock() {
        lock.unlock();
    }
}
