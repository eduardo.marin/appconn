package org.appconn;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <i>appconn</i> Types.
 */
interface Types {

    Set<Class<?>> TYPES = Collections.unmodifiableSet(Stream.of(
            // primitive
            boolean.class,
            byte.class,
            char.class,
            double.class,
            float.class,
            int.class,
            long.class,
            short.class,

            Boolean.class,
            Byte.class,
            Character.class,
            Double.class,
            Float.class,
            Integer.class,
            Long.class,
            Short.class,
            String.class,

            BigDecimal.class,
            BigInteger.class,

            Date.class,
            LocalDate.class,
            LocalDateTime.class,
            LocalTime.class,
            ZonedDateTime.class,

            Properties.class
    ).collect(Collectors.toSet()));

    static <K, V> Map.Entry<K, V> entry(K key, V value) {
        return new AbstractMap.SimpleEntry<>(key, value);
    }

    Map<Class<?>, String> GET_METHOD = Collections.unmodifiableMap(Stream.of(
            entry(boolean.class, "getBoolean"),
            entry(byte.class, "get"),
            entry(char.class, "getChar"),
            entry(double.class, "getDouble"),
            entry(float.class, "getFloat"),
            entry(int.class, "getInt"),
            entry(long.class, "getLong"),
            entry(short.class, "getShort")
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    String GET_TYPE = "TypeReader.getType";
    String GET_TYPES = "TypeReader.getTypes";
    String GET_COLLECTION = "TypeReader.getCollection";

    Map<Class<?>, String> READERS = Collections.unmodifiableMap(Stream.of(
            // primitive
            entry(boolean.class, "ReadBuffer::getBoolean"),
            entry(byte.class, "ReadBuffer::get"),
            entry(char.class, "ReadBuffer::getChar"),
            entry(double.class, "ReadBuffer::getDouble"),
            entry(float.class, "ReadBuffer::getFloat"),
            entry(int.class, "ReadBuffer::getInt"),
            entry(long.class, "ReadBuffer::getLong"),
            entry(short.class, "ReadBuffer::getShort"),
            entry(void.class, "TypeReader.VOID_READER"),

            entry(Boolean.class, "TypeReader.BOOLEAN_READER"),
            entry(Byte.class, "TypeReader.BYTE_READER"),
            entry(Character.class, "TypeReader.CHARACTER_READER"),
            entry(Double.class, "TypeReader.DOUBLE_READER"),
            entry(Float.class, "TypeReader.FLOAT_READER"),
            entry(Integer.class, "TypeReader.INTEGER_READER"),
            entry(Long.class, "TypeReader.LONG_READER"),
            entry(Short.class, "TypeReader.SHORT_READER"),
            entry(String.class, "TypeReader.STRING_READER"),

            entry(BigDecimal.class, "TypeReader.BIG_DECIMAL_READER"),
            entry(BigInteger.class, "TypeReader.BIG_INTEGER_READER"),

            entry(Date.class, "TypeReader.DATE_READER"),

            entry(Instant.class, "TypeReader.INSTANT_READER"),
            entry(LocalDate.class, "TypeReader.LOCAL_DATE_READER"),
            entry(LocalDateTime.class, "TypeReader.LOCAL_DATE_TIME_READER"),
            entry(LocalTime.class, "TypeReader.LOCAL_TIME_READER"),
            entry(ZonedDateTime.class, "TypeReader.ZONED_DATE_TIME_READER"),

            entry(Properties.class, "TypeReader.PROPERTIES_READER")
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    Map<Class<?>, String> ARRAY_READERS = Collections.unmodifiableMap(Stream.of(
            entry(byte.class, "TypeReader.BYTES_READER")
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    Map<Class<?>, String> PUT_METHOD = Collections.unmodifiableMap(Stream.of(
            entry(boolean.class, "putBoolean"),
            entry(byte.class, "put"),
            entry(char.class, "putChar"),
            entry(double.class, "putDouble"),
            entry(float.class, "putFloat"),
            entry(int.class, "putInt"),
            entry(long.class, "putLong"),
            entry(short.class, "putShort")
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    String PUT_TYPE = "TypeWriter.putType";
    String PUT_TYPES = "TypeWriter.putTypes";
    String PUT_COLLECTION = "TypeWriter.putCollection";

    Map<Class<?>, String> WRITERS = Collections.unmodifiableMap(Stream.of(
            entry(Boolean.class, "TypeWriter.BOOLEAN_WRITER"),
            entry(Byte.class, "TypeWriter.BYTE_WRITER"),
            entry(Character.class, "TypeWriter.CHARACTER_WRITER"),
            entry(Double.class, "TypeWriter.DOUBLE_WRITER"),
            entry(Float.class, "TypeWriter.FLOAT_WRITER"),
            entry(Integer.class, "TypeWriter.INTEGER_WRITER"),
            entry(Long.class, "TypeWriter.LONG_WRITER"),
            entry(Short.class, "TypeWriter.SHORT_WRITER"),
            entry(String.class, "TypeWriter.STRING_WRITER"),

            entry(BigDecimal.class, "TypeWriter.BIG_DECIMAL_WRITER"),
            entry(BigInteger.class, "TypeWriter.BIG_INTEGER_WRITER"),

            entry(Date.class, "TypeWriter.DATE_WRITER"),

            entry(Instant.class, "TypeWriter.INSTANT_WRITER"),
            entry(LocalDate.class, "TypeWriter.LOCAL_DATE_WRITER"),
            entry(LocalDateTime.class, "TypeWriter.LOCAL_DATE_TIME_WRITER"),
            entry(LocalTime.class, "TypeWriter.LOCAL_TIME_WRITER"),
            entry(ZonedDateTime.class, "TypeWriter.ZONED_DATE_TIME_WRITER"),

            entry(Properties.class, "TypeWriter.PROPERTIES_WRITER")
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    Map<Class<?>, String> ARRAY_WRITERS = Collections.unmodifiableMap(Stream.of(
            entry(byte.class, "TypeWriter.BYTES_WRITER")
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));

    Map<Class<?>, String> WRAPPERS = Collections.unmodifiableMap(Stream.of(
            entry(boolean.class, "Boolean"),
            entry(byte.class, "Byte"),
            entry(char.class, "Character"),
            entry(double.class, "Double"),
            entry(float.class, "Float"),
            entry(int.class, "Integer"),
            entry(long.class, "Long"),
            entry(short.class, "Short"),
            entry(void.class, "Void")
    ).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)));
}
