package org.appconn;

import org.appconn.channel.MethodStub;
import org.appconn.mgmt.*;

import javax.tools.*;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <i>appconn</i> access singleton class.
 * @since 1.8
 */
final public class Appconn {

    private static Appconn appconn;

    private static final long DEFAULT_STATS_TIMER_PERIOD = 60_000L;

    private File d = null;

    private AppServer appServer;
    private AppClient appClient;
    private MgmtService mgmt;

    private final Map<Integer, StatsEntry> statsMap;
    private final Lock statsMapLock;
    private Timer statsTimer;
    private long statsTimerPeriod;

    private Appconn() {
        appServer = null;
        appClient = null;
        mgmt = null;

        statsMap = new HashMap<>();
        statsMapLock = new ReentrantLock();
        statsTimer = null;
        statsTimerPeriod = DEFAULT_STATS_TIMER_PERIOD;
    }

    /**
     * Gets the <i>appconn</i> instance.
     * @return the Appconn
     */
    public static Appconn getAppconn() {
        if (appconn == null) appconn = new Appconn();
        return appconn;
    }

    /**
     * Sets the compilation directory. If d is not set, the tmpdir is used.
     * @param d the directory
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     */
    public void setDirectory(File d) throws AppconnException {
        if (d.isDirectory()) this.d = d;
        else throw new AppconnException("d must be a directory");
    }

    /**
     * Sets the stats timer period. If stats timer period is not set, the default period of 60 seconds is used.
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     */
    public void setStatsTimerPeriod(long period, TimeUnit timeUnit) throws AppconnException {
        if (getMgmt().isPresent()) throw new AppconnException("mgmt is already set");
        if (period > 0) statsTimerPeriod = timeUnit.toMillis(period);
        else throw new AppconnException("period must be positive");
    }

    /**
     * Sets the AppServer for the given serverProperties.
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     * @throws IOException      if an I/O exception of some sort has occurred
     */
    public Appconn setAppServer(ServerProperties serverProperties) throws AppconnException, IOException {
        if (appServer != null) throw new AppconnException("appServer is already set");
        if (Server.IS_ONLINE.or(Server.IS_SPARE).negate().test(serverProperties))
            throw new AppconnException("unsupported state " + serverProperties.getState());
        appServer = new AppServer(serverProperties);
        try {
            return publish(appServer);
        } catch (CoderException e) {
            appServer = null;
            throw new Error(e);
        }
    }

    /**
     * Sets the AppServer for the given id and <i>appconn</i> Mgmt address.
     * The server properties are requested to the <i>appconn</i> Mgmt.
     * @param id     the server id
     * @param remote a remote <i>appconn</i> Mgmt address
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     * @throws IOException      if an I/O exception of some sort has occurred
     */
    public Appconn setAppServer(int id, Address remote) throws AppconnException, IOException {
        try {
            return setAppServer(client(MgmtService.class, Objects.requireNonNull(remote, "remote is required")).getServerProperties(id));
        } catch (CoderException e) {
            throw new Error(e);
        }
    }

    /**
     * Sets the AppServer for the given id, name, mappingKey and local Address.
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     * @throws IOException      if an I/O exception of some sort has occurred
     */
    public Appconn setAppServer(int id, int mappingKey, String name, Address address) throws AppconnException, IOException {
        return setAppServer(new ServerProperties(id, mappingKey, name, Objects.requireNonNull(address, "address is required"), Server.STATE.ONLINE.byteValue()));
    }

    /**
     * Sets the AppServer for the given local Address. This server cannot be managed by the <i>appconn</i> Mgmt.
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     * @throws IOException      if an I/O exception of some sort has occurred
     */
    public Appconn setAppServer(Address address) throws AppconnException, IOException {
        return setAppServer(new ServerProperties(-1, -1, null, Objects.requireNonNull(address, "address is required"), Server.STATE.ONLINE.byteValue()));
    }

    /**
     * @return the local <i>appconn</i> Server.
     */
    public Optional<Server> getLocalServer() {
        return Optional.ofNullable(appServer);
    }

    /**
     * Sets the Interruptible implementation.
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     */
    public Appconn setInterruptible(Interruptible interruptible) throws AppconnException {
        if (appServer == null) throw new AppconnException("appServer is required");
        appServer.setInterruptible(interruptible);
        return appconn;
    }

    private void setAppClient() throws AppconnException {
        if (appClient == null) {
            if (appServer == null) throw new AppconnException("appServer is required");
            else if (appServer.getAddress().getHost() == null)
                throw new AppconnException("appServer with defined host is required");
            appClient = new AppClient();
            try {
                publish(appClient);
            } catch (CoderException | IOException e) {
                appClient = null;
                throw new Error(e);
            }
        }
    }

    /**
     * Publishes an <i>appconn</i> service implementation.
     * @param impl the <i>appconn</i> service implementation
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     * @throws IOException      if an I/O exception of some sort has occurred
     */
    public Appconn publish(Object impl) throws AppconnException, IOException {
        if (appServer == null)
            setAppServer(new ServerProperties(-1, -1, null, new Address(null, -1), Server.STATE.ONLINE.byteValue()));
        // services
        List<Class<?>> services = Stream.of(Objects.requireNonNull(impl, "impl is required").getClass().getInterfaces()).filter(Coders.IS_APPCONN_SERVICE).collect(Collectors.toList());
        if (services.isEmpty())
            throw new CoderException("impl must be instance of @AppconnService public interface implementation");
        else {
            for (Class<?> service : services) {
                TopologicalSet topologicalSet = new TopologicalSet(service);
                if (topologicalSet.hasCycle()) throw new CoderException("cycle " + topologicalSet.getCycleString());

                String code = Coders.stubsCode(service, topologicalSet.getSortedAppconnTypes());
                Logger.getLogger("org.appconn").log(Level.FINEST, code);

                Class<?> Stubs = compile(d, service.getName() + "Stubs", code);
                try {
                    Object stubs = Stubs.getConstructor(service).newInstance(impl);
                    for (Method m : service.getMethods()) {
                        if (Coders.IS_APPCONN_METHOD.test(m))
                            appServer.getServer().put(Coders.METHOD_KEY.apply(m), (MethodStub) Stubs.getField(Coders.STUB_NAME.apply(m)).get(stubs));
                    }
                } catch (Exception e) {
                    throw new CoderException(e.getMessage(), e);
                }
                // publish
                getMgmt().ifPresent(mgmt -> {
                    if (!service.getName().equals(AppClientService.class.getName()))
                        try {
                            mgmt.publish(service.getName(), appServer.getId());
                        } catch (MgmtException e) {
                            throw new Error(e);
                        }
                });
            }
        }
        return appconn;
    }

    @SuppressWarnings("unchecked")
    private <T> T client(Class<T> service, boolean extendsServiceClient, Client.CONNS_TYPE connsType, ConnGetter connGetter, Address remote) throws CoderException {
        if (service.isInterface() && Coders.IS_APPCONN_SERVICE.test(service)) {
            // topologicalSet
            TopologicalSet topologicalSet = new TopologicalSet(service);
            if (topologicalSet.hasCycle()) throw new CoderException("cycle " + topologicalSet.getCycleString());

            String code = Coders.clientCode(service, topologicalSet.getSortedAppconnTypes(), extendsServiceClient, connsType);
            Logger.getLogger("org.appconn").log(Level.FINEST, code);

            try {
                return (T) compile(d, Coders.clientClassName(service, extendsServiceClient, connsType, false), code).getConstructor(extendsServiceClient ? ConnGetter.class : SocketAddress.class).newInstance(extendsServiceClient ? connGetter : remote.toSocketAddress());
            } catch (Exception e) {
                throw new CoderException(e.getMessage(), e);
            }
        } else throw new CoderException(service.getName() + " should be @AppconnService annotated interface");
    }

    private <T> T client(Class<T> service, Client.CONNS_TYPE connsType, ConnGetter connGetter) throws AppconnException {
        setAppClient();
        appClient.lock();
        try {
            if (appClient.getServiceClient(service).isPresent())
                throw new AppconnException(service.getName() + " client is present");
            else {
                MgmtService mgmtService = getMgmt().orElseThrow(() -> new AppconnException("mgmt is required"));
                T serviceClient = client(service, true, connsType, connGetter, null);
                appClient.putServiceClient(service.getName(), (ServiceClient) serviceClient);
                appClient.setServers(service.getName(), mgmtService.getServers(service.getName()));
                mgmtService.addClient(service.getName(), new Client(appServer.getAddress(), connsType.byteValue()));
                return serviceClient;
            }
        } finally {
            appClient.unlock();
        }
    }

    /**
     * Constructs a new {@code CONN} client for the given service and ConnGetter.
     * @param service    the <i>appconn</i> service interface
     * @param connGetter the ConnGetter
     * @param <T>        the <i>appconn</i> service type
     * @return the <i>appconn</i> service instance
     * @throws AppconnException if client is present or <i>appconn</i> requirements are not satisfied
     */
    public <T> T connClient(Class<T> service, ConnGetter connGetter) throws AppconnException {
        return client(Objects.requireNonNull(service, "service is required"), Client.CONNS_TYPE.CONN, Objects.requireNonNull(connGetter, "connGetter is required"));
    }

    /**
     * Constructs a new {@code CONN} client for the given service and {@code FIRST} ConnGetter.
     * @param service the <i>appconn</i> service interface
     * @param <T>     the <i>appconn</i> service type
     * @return the <i>appconn</i> service instance
     * @throws AppconnException if client is present or <i>appconn</i> requirements are not satisfied
     */
    public <T> T connClient(Class<T> service) throws AppconnException {
        return client(Objects.requireNonNull(service, "service is required"), Client.CONNS_TYPE.CONN, ConnGetter.FIRST);
    }

    /**
     * Constructs a new {@code CONNLESS} client for the given service and ConnGetter.
     * @param service    the <i>appconn</i> service interface
     * @param connGetter the ConnGetter
     * @param <T>        the <i>appconn</i> service type
     * @return the <i>appconn</i> service instance
     * @throws AppconnException if client is present or <i>appconn</i> requirements are not satisfied
     */
    public <T> T connlessClient(Class<T> service, ConnGetter connGetter) throws AppconnException {
        return client(Objects.requireNonNull(service, "service is required"), Client.CONNS_TYPE.CONNLESS, Objects.requireNonNull(connGetter, "connGetter is required"));
    }

    /**
     * Constructs a new {@code CONNLESS} client for the given service and {@code FIRST} ConnGetter.
     * @param service the <i>appconn</i> service interface
     * @param <T>     the <i>appconn</i> service type
     * @return the <i>appconn</i> service instance
     * @throws AppconnException if client is present or <i>appconn</i> requirements are not satisfied
     */
    public <T> T connlessClient(Class<T> service) throws AppconnException {
        return client(Objects.requireNonNull(service, "service is required"), Client.CONNS_TYPE.CONNLESS, ConnGetter.FIRST);
    }

    /**
     * Constructs a new client for the given service and remote server Address.
     * This client cannot be managed by the <i>appconn</i> Mgmt.
     * @param service the <i>appconn</i> service interface
     * @param remote  the server Address
     * @param <T>     the <i>appconn</i> service type
     * @return the <i>appconn</i> service instance
     * @throws CoderException if the <i>appconn</i> service cannot be instantiated
     */
    public <T> T client(Class<T> service, Address remote) throws CoderException {
        return client(Objects.requireNonNull(service, "service is required"), false, null, null, Objects.requireNonNull(remote, "remote is required"));
    }

    /**
     * Publishes the <i>appconn</i> Mgmt locally.
     * @param serverProperties the server properties
     * @param url              the Mgmt database url
     * @param user             the Mgmt database user
     * @param password         the Mgmt database password
     * @param remote           a remote <i>appconn</i> Mgmt Address
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     * @throws IOException      if an I/O exception of some sort has occurred
     */
    public Appconn publishMgmt(ServerProperties serverProperties, String url, String user, String password, Address remote) throws AppconnException, IOException {
        if (getMgmt().isPresent()) throw new AppconnException("mgmt is already set");

        SortedSet<Server> servers = new TreeSet<>();
        SortedSet<Service> services = new TreeSet<>();
        if (remote != null) {
            try {
                MgmtService mgmtService = client(MgmtService.class, remote);
                if (Server.IS_ONLINE.test(Objects.requireNonNull(serverProperties, "serverProperties is required")) && !mgmtService.getServers(MgmtService.class.getName()).isEmpty())
                    serverProperties.setState(Server.STATE.SPARE.byteValue());

                servers = new TreeSet<>(mgmtService.getServers());
                services = new TreeSet<>(mgmtService.getServices());
            } catch (CoderException e) {
                throw new Error(e);
            } catch (Exception e) {
                Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
            }
        } else serverProperties.setState(Server.STATE.ONLINE.byteValue());

        setAppServer(serverProperties);
        mgmt = new Mgmt(appServer, servers, services, url, user, password);
        try {
            mgmt.addServer(appServer);
            publish(mgmt);
        } catch (CoderException e) {
            throw new Error(e);
        }
        // statsTimer
        statsTimer = new Timer(getClass().getName());
        statsTimer.scheduleAtFixedRate(new StatsTimerTask(), statsTimerPeriod, statsTimerPeriod);
        return appconn;
    }

    private Optional<MgmtService> getMgmt() {
        if (mgmt != null) return Optional.of(mgmt);
        else if (appClient != null) return appClient.getServiceClient(MgmtService.class);
        return Optional.empty();
    }

    /**
     * @return the local <i>appconn</i> Mgmt.
     */
    public Optional<MgmtOpsService> getLocalMgmt() {
        return Optional.ofNullable(mgmt);
    }

    /**
     * Sets the <i>appconn</i> Mgmt for the given remote Address.
     * @param remote a remote <i>appconn</i> Mgmt Address
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     */
    public Appconn setMgmt(Address remote) throws AppconnException {
        setAppClient();
        appClient.lock();
        try {
            if (getMgmt().isPresent()) throw new AppconnException("mgmt is already set");
            MgmtService mgmtService = client(MgmtService.class, true, Client.CONNS_TYPE.CONNLESS, ConnGetter.FIRST, null);
            appClient.putServiceClient(MgmtService.class.getName(), (ServiceClient) mgmtService);
            appClient.setServers(MgmtService.class.getName(), client(MgmtService.class, Objects.requireNonNull(remote, "remote is required")).getServers(MgmtService.class.getName()));
            mgmtService.addClient(MgmtService.class.getName(), new Client(appServer.getAddress(), Client.CONNS_TYPE.CONNLESS.byteValue()));
            if (appServer.getId() >= 0) mgmtService.addServer(appServer);
        } catch (CoderException e) {
            throw new Error(e);
        } finally {
            appClient.unlock();
        }
        // statsTimer
        statsTimer = new Timer(getClass().getName());
        statsTimer.scheduleAtFixedRate(new StatsTimerTask(), statsTimerPeriod, statsTimerPeriod);
        return appconn;
    }

    /**
     * Sets the StatsHandler implementation.
     * @return the Appconn
     * @throws AppconnException if <i>appconn</i> requirements are not satisfied
     */
    public Appconn setStatsHandler(StatsHandler statsHandler) throws AppconnException {
        if (mgmt == null) throw new AppconnException("local mgmt is required");
        ((Mgmt) mgmt).setStatsHandler(statsHandler);
        return appconn;
    }

    /**
     * Adds the stat for the given key, n and nanos.
     */
    public void addStat(int key, int n, long nanos) {
        long tmpMillis = nanos / 1_000_000L;
        long tmpNanos = nanos - (tmpMillis * 1_000_000L);
        // round
        if (tmpNanos > 500_000L || (tmpNanos == 500_000L && (tmpMillis & 1) == 1)) ++tmpMillis;
        final long millis = tmpMillis;
        statsMapLock.lock();
        try {
            statsMap.compute(key, (k, v) -> v != null ? v.add(n, millis, millis * millis)
                                                      : new StatsEntry(k, n, millis, millis * millis));
        } finally {
            statsMapLock.unlock();
        }
    }

    /**
     * Clears the <i>appconn</i>. It removes the server and all created clients.
     */
    public void clear() {
        if (appServer != null) {
            if (appServer.getId() >= 0) getMgmt().ifPresent(mgmt -> mgmt.removeServer(appServer.getId()));
            appServer.setState(Server.STATE.OFFLINE.byteValue());
        }
        if (appClient != null) {
            appClient.lock();
            try {
                appClient.getServiceNames()
                         .forEach(serviceName -> getMgmt().ifPresent(mgmt -> mgmt.removeClient(serviceName, new Client(appServer.getAddress(),
                                                                                                                       appClient.getServiceClient(serviceName).getConnsType().byteValue()))));
                appClient.clear();
            } finally {
                appClient.unlock();
            }
        }
        if (statsTimer != null) statsTimer.cancel();
        // statsMap
        statsMapLock.lock();
        try {
            statsMap.clear();
        } finally {
            statsMapLock.unlock();
        }
        if (mgmt != null) ((Mgmt) mgmt).clear();
        appServer = null;
        appClient = null;
        mgmt = null;
        statsTimer = null;
    }

    private Class<?> compile(File d, String name, String code) throws CoderException {
        if (d == null) d = new File(System.getProperty("java.io.tmpdir"));
        Logger.getLogger("org.appconn").log(Level.FINER, "-d " + d);

        DiagnosticCollector<JavaFileObject> diagnosticListener = new DiagnosticCollector<>();
        Iterable<String> options = Arrays.asList("-d", d.getAbsolutePath());
        Iterable<? extends JavaFileObject> compilationUnits = Collections.singletonList(new CodeObject(name, code));

        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        if (compiler.getTask(null, null, diagnosticListener, options, null, compilationUnits).call()) {
            Logger.getLogger("org.appconn").log(Level.FINE, "compile " + name);
            try {
                URLClassLoader urlClassLoader = new URLClassLoader(new URL[]{d.toURI().toURL()});
                return Class.forName(name, true, urlClassLoader);
            } catch (MalformedURLException | ClassNotFoundException e) {
                throw new CoderException(e.getMessage(), e);
            }
        } else {
            StringBuilder message = new StringBuilder();
            for (Diagnostic diagnostic : diagnosticListener.getDiagnostics())
                message.append(diagnostic.getMessage(null));
            throw new CoderException(message.toString());
        }
    }

    private class CodeObject extends SimpleJavaFileObject {

        private final String code;

        private CodeObject(String name, String code) {
            super(URI.create("string:/" + name.replace('.', '/') + Kind.SOURCE.extension), Kind.SOURCE);
            this.code = code;
        }

        @Override
        public CharSequence getCharContent(boolean ignoreEncodingErrors) throws IOException {
            return code;
        }
    }

    private class StatsTimerTask extends TimerTask {

        @Override
        public void run() {
            getMgmt().ifPresent(mgmt -> {
                statsMapLock.lock();
                try {
                    Collection<StatsEntry> stats = statsMap.values()
                                                           .stream()
                                                           .filter(s -> s.getN() > 0 || s.getMillisSum() > 0 || s.getMillisSquaredSum() > 0)
                                                           .collect(Collectors.toList());
                    statsMap.clear();
                    if (!stats.isEmpty()) mgmt.addStats(appServer.getId(), stats);
                } finally {
                    statsMapLock.unlock();
                }
            });
        }
    }
}
