package org.appconn.channel;

import java.io.IOException;

/**
 * <i>appconn</i> MethodStub functional interface.
 */
@FunctionalInterface
public interface MethodStub {

    /**
     * @param readBuffer  the ReadBuffer
     * @param writeBuffer the WriteBuffer
     * @param position    the position
     * @return the Runnable
     * @throws IOException if an I/O exception of some sort has occurred
     */
    Runnable runnable(ReadBuffer readBuffer, WriteBuffer writeBuffer, int position) throws IOException;
}
