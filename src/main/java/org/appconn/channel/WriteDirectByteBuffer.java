package org.appconn.channel;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <i>appconn</i> WriteBuffer <em>Direct</em> implementation.
 */
final class WriteDirectByteBuffer implements WriteBuffer {

    private final WritableByteChannel channel;

    private final ByteBuffer byteBuffer;

    private final Lock lock;

    WriteDirectByteBuffer(WritableByteChannel channel, int length) {
        this.channel = channel;
        byteBuffer = ByteBuffer.allocateDirect(length);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
        lock = new ReentrantLock();
    }

    @Override
    public WriteBuffer put(byte value) throws IOException {
        if (byteBuffer.remaining() < 1) flush();
        byteBuffer.put(value);
        return this;
    }

    @Override
    public WriteBuffer putBytes(byte[] value) throws IOException {
        putInt(value.length);
        int index = 0;
        for (int remaining = byteBuffer.remaining(); value.length - index > remaining; remaining = byteBuffer.remaining()) {
            byteBuffer.put(value, index, remaining);
            index += remaining;
            flush();
        }
        byteBuffer.put(value, index, value.length - index);
        return this;
    }

    @Override
    public WriteBuffer putChar(char value) throws IOException {
        if (byteBuffer.remaining() < 2) flush();
        byteBuffer.putChar(value);
        return this;
    }

    @Override
    public WriteBuffer putDouble(double value) throws IOException {
        if (byteBuffer.remaining() < 8) flush();
        byteBuffer.putDouble(value);
        return this;
    }

    @Override
    public WriteBuffer putFloat(float value) throws IOException {
        if (byteBuffer.remaining() < 4) flush();
        byteBuffer.putFloat(value);
        return this;
    }

    @Override
    public WriteBuffer putInt(int value) throws IOException {
        if ((value & ~0x7f) == 0) {
            if (byteBuffer.remaining() < 1) flush();
            byteBuffer.put((byte) value);
        } else if ((value & ~0x3fff) == 0) {
            if (byteBuffer.remaining() < 2) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7));
        } else if ((value & ~0x1fffff) == 0) {
            if (byteBuffer.remaining() < 3) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14));
        } else if ((value & ~0xfffffff) == 0) {
            if (byteBuffer.remaining() < 4) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21));
        } else {
            if (byteBuffer.remaining() < 5) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21 | 0x80));
            byteBuffer.put((byte) (value >>> 28));
        }
        return this;
    }

    @Override
    public WriteBuffer putLong(long value) throws IOException {
        if ((value & ~0x7f) == 0) {
            if (byteBuffer.remaining() < 1) flush();
            byteBuffer.put((byte) value);
        } else if ((value & ~0x3fff) == 0) {
            if (byteBuffer.remaining() < 2) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7));
        } else if ((value & ~0x1fffff) == 0) {
            if (byteBuffer.remaining() < 3) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14));
        } else if ((value & ~0xfffffff) == 0) {
            if (byteBuffer.remaining() < 4) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21));
        } else if ((value & ~0x7ffffffffL) == 0) {
            if (byteBuffer.remaining() < 5) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21 | 0x80));
            byteBuffer.put((byte) (value >>> 28));
        } else if ((value & ~0x3ffffffffffL) == 0) {
            if (byteBuffer.remaining() < 6) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21 | 0x80));
            byteBuffer.put((byte) (value >>> 28 | 0x80));
            byteBuffer.put((byte) (value >>> 35));
        } else if ((value & ~0x1ffffffffffffL) == 0) {
            if (byteBuffer.remaining() < 7) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21 | 0x80));
            byteBuffer.put((byte) (value >>> 28 | 0x80));
            byteBuffer.put((byte) (value >>> 35 | 0x80));
            byteBuffer.put((byte) (value >>> 42));
        } else if ((value & ~0xffffffffffffffL) == 0) {
            if (byteBuffer.remaining() < 8) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21 | 0x80));
            byteBuffer.put((byte) (value >>> 28 | 0x80));
            byteBuffer.put((byte) (value >>> 35 | 0x80));
            byteBuffer.put((byte) (value >>> 42 | 0x80));
            byteBuffer.put((byte) (value >>> 49));
        } else if ((value & ~0x7fffffffffffffffL) == 0) {
            if (byteBuffer.remaining() < 9) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21 | 0x80));
            byteBuffer.put((byte) (value >>> 28 | 0x80));
            byteBuffer.put((byte) (value >>> 35 | 0x80));
            byteBuffer.put((byte) (value >>> 42 | 0x80));
            byteBuffer.put((byte) (value >>> 49 | 0x80));
            byteBuffer.put((byte) (value >>> 56));
        } else {
            if (byteBuffer.remaining() < 10) flush();
            byteBuffer.put((byte) (value | 0x80));
            byteBuffer.put((byte) (value >>> 7 | 0x80));
            byteBuffer.put((byte) (value >>> 14 | 0x80));
            byteBuffer.put((byte) (value >>> 21 | 0x80));
            byteBuffer.put((byte) (value >>> 28 | 0x80));
            byteBuffer.put((byte) (value >>> 35 | 0x80));
            byteBuffer.put((byte) (value >>> 42 | 0x80));
            byteBuffer.put((byte) (value >>> 49 | 0x80));
            byteBuffer.put((byte) (value >>> 56 | 0x80));
            byteBuffer.put((byte) (value >>> 63));
        }
        return this;
    }

    @Override
    public WriteBuffer putShort(short value) throws IOException {
        if (byteBuffer.remaining() < 2) flush();
        byteBuffer.putShort(value);
        return this;
    }

    @Override
    public void flush() throws IOException {
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) channel.write(byteBuffer);
        byteBuffer.clear();
    }

    @Override
    public void lock() {
        lock.lock();
    }

    @Override
    public void unlock() {
        lock.unlock();
    }
}
