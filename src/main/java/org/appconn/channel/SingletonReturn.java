package org.appconn.channel;

/**
 * <i>appconn</i> SingletonReturn implementation.
 */
final class SingletonReturn implements Returns {

    private MethodReturn<?> methodReturn;

    /**
     * Constructs a new SingletonReturn.
     */
    SingletonReturn() {
        methodReturn = null;
    }

    @Override
    public int add(MethodReturn<?> methodReturn) throws ChannelException {
        if (this.methodReturn == null) this.methodReturn = methodReturn;
        else throw new ChannelException("methodReturn not null");
        return 0;
    }

    @Override
    public MethodReturn<?> remove(int position) {
        MethodReturn<?> tmpMethodReturn = null;
        if (position == 0) {
            tmpMethodReturn = methodReturn;
            methodReturn = null;
        }
        return tmpMethodReturn;
    }
}
