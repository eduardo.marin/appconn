package org.appconn.channel;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * <i>appconn</i> WriteBuffer <em>Heap</em> implementation.
 */
class WriteHeapByteBuffer implements WriteBuffer {

    protected final WritableByteChannel channel;

    byte[] buffer;
    int position;

    ByteBuffer byteBuffer;

    private final Lock lock;

    protected WriteHeapByteBuffer(WritableByteChannel channel) {
        this.channel = channel;
        buffer = null;
        position = 0;
        byteBuffer = null;
        lock = new ReentrantLock();
    }

    WriteHeapByteBuffer(WritableByteChannel channel, int length) {
        this(channel);
        buffer = new byte[length];
        byteBuffer = ByteBuffer.wrap(buffer);
    }

    @Override
    public WriteBuffer put(byte value) throws IOException {
        if (position == buffer.length) flush();
        buffer[position++] = value;
        return this;
    }

    @Override
    public WriteBuffer putBytes(byte[] value) throws IOException {
        putInt(value.length);
        int index = 0;
        for (int remaining = buffer.length - position; value.length - index > remaining; remaining = buffer.length - position) {
            System.arraycopy(value, index, buffer, position, remaining);
            position += remaining;
            index += remaining;
            flush();
        }
        System.arraycopy(value, index, buffer, position, value.length - index);
        position += value.length - index;
        return this;
    }

    @Override
    public WriteBuffer putChar(char value) throws IOException {
        if (position + 2 > buffer.length) flush();
        buffer[position] = (byte) value;
        buffer[position + 1] = (byte) (value >> 8);
        position += 2;
        return this;
    }

    @Override
    public WriteBuffer putDouble(double value) throws IOException {
        if (position + 8 > buffer.length) flush();
        long bits = Double.doubleToRawLongBits(value);
        buffer[position] = (byte) bits;
        buffer[position + 1] = (byte) (bits >> 8);
        buffer[position + 2] = (byte) (bits >> 16);
        buffer[position + 3] = (byte) (bits >> 24);
        buffer[position + 4] = (byte) (bits >> 32);
        buffer[position + 5] = (byte) (bits >> 40);
        buffer[position + 6] = (byte) (bits >> 48);
        buffer[position + 7] = (byte) (bits >> 56);
        position += 8;
        return this;
    }

    @Override
    public WriteBuffer putFloat(float value) throws IOException {
        if (position + 4 > buffer.length) flush();
        int bits = Float.floatToRawIntBits(value);
        buffer[position] = (byte) bits;
        buffer[position + 1] = (byte) (bits >> 8);
        buffer[position + 2] = (byte) (bits >> 16);
        buffer[position + 3] = (byte) (bits >> 24);
        position += 4;
        return this;
    }

    @Override
    public WriteBuffer putInt(int value) throws IOException {
        if ((value & ~0x7f) == 0) {
            if (position == buffer.length) flush();
            buffer[position++] = (byte) value;
        } else if ((value & ~0x3fff) == 0) {
            if (position + 2 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7);
            position += 2;
        } else if ((value & ~0x1fffff) == 0) {
            if (position + 3 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14);
            position += 3;
        } else if ((value & ~0xfffffff) == 0) {
            if (position + 4 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21);
            position += 4;
        } else {
            if (position + 5 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21 | 0x80);
            buffer[position + 4] = (byte) (value >>> 28);
            position += 5;
        }
        return this;
    }

    @Override
    public WriteBuffer putLong(long value) throws IOException {
        if ((value & ~0x7f) == 0) {
            if (position == buffer.length) flush();
            buffer[position++] = (byte) value;
        } else if ((value & ~0x3fff) == 0) {
            if (position + 2 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7);
            position += 2;
        } else if ((value & ~0x1fffff) == 0) {
            if (position + 3 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14);
            position += 3;
        } else if ((value & ~0xfffffff) == 0) {
            if (position + 4 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21);
            position += 4;
        } else if ((value & ~0x7ffffffffL) == 0) {
            if (position + 5 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21 | 0x80);
            buffer[position + 4] = (byte) (value >>> 28);
            position += 5;
        } else if ((value & ~0x3ffffffffffL) == 0) {
            if (position + 6 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21 | 0x80);
            buffer[position + 4] = (byte) (value >>> 28 | 0x80);
            buffer[position + 5] = (byte) (value >>> 35);
            position += 6;
        } else if ((value & ~0x1ffffffffffffL) == 0) {
            if (position + 7 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21 | 0x80);
            buffer[position + 4] = (byte) (value >>> 28 | 0x80);
            buffer[position + 5] = (byte) (value >>> 35 | 0x80);
            buffer[position + 6] = (byte) (value >>> 42);
            position += 7;
        } else if ((value & ~0xffffffffffffffL) == 0) {
            if (position + 8 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21 | 0x80);
            buffer[position + 4] = (byte) (value >>> 28 | 0x80);
            buffer[position + 5] = (byte) (value >>> 35 | 0x80);
            buffer[position + 6] = (byte) (value >>> 42 | 0x80);
            buffer[position + 7] = (byte) (value >>> 49);
            position += 8;
        } else if ((value & ~0x7fffffffffffffffL) == 0) {
            if (position + 9 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21 | 0x80);
            buffer[position + 4] = (byte) (value >>> 28 | 0x80);
            buffer[position + 5] = (byte) (value >>> 35 | 0x80);
            buffer[position + 6] = (byte) (value >>> 42 | 0x80);
            buffer[position + 7] = (byte) (value >>> 49 | 0x80);
            buffer[position + 8] = (byte) (value >>> 56);
            position += 9;
        } else {
            if (position + 10 > buffer.length) flush();
            buffer[position] = (byte) (value | 0x80);
            buffer[position + 1] = (byte) (value >>> 7 | 0x80);
            buffer[position + 2] = (byte) (value >>> 14 | 0x80);
            buffer[position + 3] = (byte) (value >>> 21 | 0x80);
            buffer[position + 4] = (byte) (value >>> 28 | 0x80);
            buffer[position + 5] = (byte) (value >>> 35 | 0x80);
            buffer[position + 6] = (byte) (value >>> 42 | 0x80);
            buffer[position + 7] = (byte) (value >>> 49 | 0x80);
            buffer[position + 8] = (byte) (value >>> 56 | 0x80);
            buffer[position + 9] = (byte) (value >>> 63);
            position += 10;
        }
        return this;
    }

    @Override
    public WriteBuffer putShort(short value) throws IOException {
        if (position + 2 > buffer.length) flush();
        buffer[position] = (byte) value;
        buffer[position + 1] = (byte) (value >> 8);
        position += 2;
        return this;
    }

    @Override
    public void flush() throws IOException {
        byteBuffer.limit(position);
        while (byteBuffer.hasRemaining()) channel.write(byteBuffer);
        position = 0;
        byteBuffer.clear();
    }

    @Override
    public void lock() {
        lock.lock();
    }

    @Override
    public void unlock() {
        lock.unlock();
    }
}
