package org.appconn.channel;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.*;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * <i>appconn</i> TypeWriter functional interface.
 * @param <T> the type
 */
@FunctionalInterface
public interface TypeWriter<T> {

    /**
     * Puts a type.
     * @param w the WriteBuffer
     * @param t the type
     * @throws IOException if an I/O exception of some sort has occurred
     */
    void put(WriteBuffer w, T t) throws IOException;

    /**
     * Puts an <i>appconn</i> type.
     * @param writeBuffer the WriteBuffer
     * @param t           the type
     * @param typeWriter  the TypeWriter
     * @param <T>         the type
     * @throws IOException if an I/O exception of some sort has occurred
     */
    static <T> void putType(WriteBuffer writeBuffer, T t, TypeWriter<T> typeWriter) throws IOException {
        if (t != null) {
            writeBuffer.put(Type.IS_NOT_NULL);
            typeWriter.put(writeBuffer, t);
        } else writeBuffer.put(Type.IS_NULL);
    }

    /**
     * Puts a type array.
     * @param writeBuffer the WriteBuffer
     * @param types       the type array
     * @param typeWriter  the TypeWriter
     * @param <T>         the type
     * @throws IOException if an I/O exception of some sort has occurred
     */
    static <T> void putTypes(WriteBuffer writeBuffer, T[] types, TypeWriter<T> typeWriter) throws IOException {
        if (types != null) {
            writeBuffer.put(Type.IS_NOT_NULL);
            writeBuffer.putInt(types.length);
            for (T t : types) putType(writeBuffer, t, typeWriter);
        } else writeBuffer.put(Type.IS_NULL);
    }

    /**
     * Puts a type Collection.
     * @param writeBuffer the WriteBuffer
     * @param types       the type Collection
     * @param typeWriter  the TypeWriter
     * @param <T>         the type
     * @throws IOException if an I/O exception of some sort has occurred
     */
    static <T> void putCollection(WriteBuffer writeBuffer, Collection<T> types, TypeWriter<T> typeWriter) throws IOException {
        if (types != null) {
            writeBuffer.put(Type.IS_NOT_NULL);
            writeBuffer.putInt(types.size());
            for (T t : types) putType(writeBuffer, t, typeWriter);
        } else writeBuffer.put(Type.IS_NULL);
    }

    TypeWriter<byte[]> BYTES_WRITER = (w, t) -> putType(w, t, WriteBuffer::putBytes);

    TypeWriter<Boolean> BOOLEAN_WRITER = (w, t) -> putType(w, t, WriteBuffer::putBoolean);
    TypeWriter<Byte> BYTE_WRITER = (w, t) -> putType(w, t, WriteBuffer::put);
    TypeWriter<Character> CHARACTER_WRITER = (w, t) -> putType(w, t, WriteBuffer::putChar);
    TypeWriter<Double> DOUBLE_WRITER = (w, t) -> putType(w, t, WriteBuffer::putDouble);
    TypeWriter<Float> FLOAT_WRITER = (w, t) -> putType(w, t, WriteBuffer::putFloat);
    TypeWriter<Integer> INTEGER_WRITER = (w, t) -> putType(w, t, WriteBuffer::putInt);
    TypeWriter<Long> LONG_WRITER = (w, t) -> putType(w, t, WriteBuffer::putLong);
    TypeWriter<Short> SHORT_WRITER = (w, t) -> putType(w, t, WriteBuffer::putShort);
    TypeWriter<String> STRING_WRITER = (w, t) -> putType(w, t, WriteBuffer::putString);

    TypeWriter<BigDecimal> BIG_DECIMAL_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        w.putBytes(t.unscaledValue().toByteArray());
        w.putInt(t.scale());
    });
    TypeWriter<BigInteger> BIG_INTEGER_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> w.putBytes(t.toByteArray()));

    TypeWriter<Date> DATE_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> w.putLong(t.getTime()));

    TypeWriter<Instant> INSTANT_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        w.putLong(t.getEpochSecond());
        w.putLong(t.getNano());
    });
    TypeWriter<LocalDate> LOCAL_DATE_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        w.putInt(t.getYear());
        w.putInt(t.getMonthValue());
        w.putInt(t.getDayOfMonth());
    });
    TypeWriter<LocalDateTime> LOCAL_DATE_TIME_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        w.putInt(t.getYear());
        w.putInt(t.getMonthValue());
        w.putInt(t.getDayOfMonth());
        w.putInt(t.getHour());
        w.putInt(t.getMinute());
        w.putInt(t.getSecond());
        w.putInt(t.getNano());
    });
    TypeWriter<LocalTime> LOCAL_TIME_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        w.putInt(t.getHour());
        w.putInt(t.getMinute());
        w.putInt(t.getSecond());
        w.putInt(t.getNano());
    });
    TypeWriter<ZonedDateTime> ZONED_DATE_TIME_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        w.putInt(t.getYear());
        w.putInt(t.getMonthValue());
        w.putInt(t.getDayOfMonth());
        w.putInt(t.getHour());
        w.putInt(t.getMinute());
        w.putInt(t.getSecond());
        w.putInt(t.getNano());
        w.putString(t.getZone().getId());
    });

    TypeWriter<Type.StringsEntry> STRINGS_ENTRY_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        STRING_WRITER.put(w, t.getKey());
        STRING_WRITER.put(w, t.getValue());
    });

    TypeWriter<Properties> PROPERTIES_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> putCollection(w, t.stringPropertyNames()
                                                                                                                             .stream()
                                                                                                                             .map(e -> new Type.StringsEntry(e, t.getProperty(e)))
                                                                                                                             .collect(Collectors.toList()), STRINGS_ENTRY_WRITER));

    TypeWriter<StackTraceElement> STACK_TRACE_ELEMENT_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        STRING_WRITER.put(w, t.getClassName());
        STRING_WRITER.put(w, t.getMethodName());
        STRING_WRITER.put(w, t.getFileName());
        w.putInt(t.getLineNumber());
    });

    TypeWriter<Throwable> THROWABLE_WRITER = (writeBuffer, type) -> putType(writeBuffer, type, (w, t) -> {
        STRING_WRITER.put(w, t.getMessage());
        putTypes(w, t.getStackTrace(), STACK_TRACE_ELEMENT_WRITER);
    });
}
