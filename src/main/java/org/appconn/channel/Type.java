package org.appconn.channel;

/**
 * <i>appconn</i> Type interface.
 */
interface Type {

    byte IS_NOT_NULL = 0;
    byte IS_NULL = 1;

    class StringsEntry {

        private final String key;
        private final String value;

        StringsEntry(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }
    }
}
