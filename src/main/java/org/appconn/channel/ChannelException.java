package org.appconn.channel;

import org.appconn.AppconnException;

/**
 * Thrown at <i>appconn</i> channel.
 */
public class ChannelException extends AppconnException {

    ChannelException(String message) {
        super(message);
    }

    ChannelException(String message, Throwable cause) {
        super(message, cause);
    }
}
