package org.appconn.channel;

import java.io.Closeable;
import java.io.IOException;
import java.net.SocketAddress;

/**
 * <i>appconn</i> AbstractClient.
 */
public abstract class AbstractClient extends Thread implements Closeable {

    Returns returns;
    ReadBuffer readBuffer;
    WriteBuffer writeBuffer;

    /**
     * AbstractClient constructor.
     * @param name the name of the thread
     */
    AbstractClient(String name) {
        super(name);
    }

    /**
     * Adds a FutureReturn to returns.
     * @return the FutureReturn position
     * @throws ChannelException if a ChannelException of some sort has occurred
     */
    public int add(FutureReturn<?> futureReturn) throws ChannelException {
        return returns.add((MethodReturn<?>) futureReturn);
    }

    /**
     * Removes the MethodReturn at given position.
     * @return the removed MethodReturn
     */
    MethodReturn<?> remove(int position) {
        return returns.remove(position);
    }

    /**
     * @return the WriteBuffer
     */
    public WriteBuffer getWriteBuffer() {
        return writeBuffer;
    }

    /**
     * Returns a new FutureReturn implementation instance.
     * @param reader  the return TypeReader
     * @param timeout the timeout in nanos
     * @param <V>     the return type
     * @return the FutureReturn
     */
    public static <V> FutureReturn<V> futureReturn(TypeReader<V> reader, Long timeout) {
        return new MethodReturn<>(reader, timeout);
    }

    /**
     * Returns a new abstract client instance with concurrent returns.
     * @param remote         the remote SocketAddress
     * @param connectTimeout the connectTimeout in millis
     * @return the AbstractClient
     * @throws IOException if an I/O exception of some sort has occurred
     */
    public static AbstractClient newTcpClient(SocketAddress remote, int connectTimeout) throws IOException {
        return new TcpClient(remote, connectTimeout, true, ReadBuffer.DEFAULT_LENGTH, WriteBuffer.DEFAULT_LENGTH, new ReturnsArray());
    }

    /**
     * Returns a new AbstractClient instance with SingletonReturn.
     * @param remote         the remote SocketAddress
     * @param connectTimeout the connectTimeout in millis
     * @return the AbstractClient
     * @throws IOException if an I/O exception of some sort has occurred
     */
    public static AbstractClient newSingletonReturnTcpClient(SocketAddress remote, int connectTimeout) throws IOException {
        return new TcpClient(remote, connectTimeout, false, ReadBuffer.SMALL_LENGTH, WriteBuffer.SMALL_LENGTH, new SingletonReturn());
    }
}
