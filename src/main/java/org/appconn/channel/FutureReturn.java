package org.appconn.channel;

import java.util.concurrent.TimeoutException;

/**
 * <i>appconn</i> FutureReturn interface.
 * @param <V> the return type
 */
public interface FutureReturn<V> {

    /**
     * @return true if the method call is done
     */
    boolean isDone();

    /**
     * @return the return of a method call
     * @throws InterruptedException if an InterruptedException occurred
     * @throws TimeoutException     if a TimeoutException occurred
     */
    V getReturn() throws Exception;
}
