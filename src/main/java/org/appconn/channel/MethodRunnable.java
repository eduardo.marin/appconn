package org.appconn.channel;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <i>appconn</i> MethodRunnable.
 */
public abstract class MethodRunnable implements Runnable {

    protected final WriteBuffer writeBuffer;
    protected final int position;
    protected final Long deadline;

    /**
     * MethodRunnable constructor.
     * @param writeBuffer the WriteBuffer
     * @param position    the position
     * @param timeout     the timeout in nanos
     */
    protected MethodRunnable(WriteBuffer writeBuffer, int position, Long timeout) {
        this.writeBuffer = writeBuffer;
        this.position = position;
        deadline = timeout != null ? System.nanoTime() + timeout : null;
    }

    /**
     * @throws Exception if an exception of some sort occurred
     */
    protected abstract void execute() throws Exception;

    @Override
    public void run() {
        try {
            execute();
        } catch (IOException e) {
            Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
        } catch (Exception e) {
            Writer.putException(writeBuffer, position, deadline, e);
        }
    }
}
