package org.appconn.channel;

import java.io.IOException;
import java.net.SocketAddress;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <i>appconn</i> AbstractServer TCP implementation.
 */
final public class TcpServer extends AbstractServer {

    public static final int DEFAULT_BACKLOG = 1 << 10;  // 1024

    private final ServerSocketChannel serverSocketChannel;

    private final ServerSelector[] selectors;
    private int selectorsIndex;

    /**
     * Constructs a new TcpServer.
     * @param local           the local SocketAddress
     * @param backlog         the maximum number of pending connections
     * @param selectorsLength the number of server selectors
     * @param poolSize        the poolSize for ThreadPoolExecutor
     * @param queueCapacity   the queueCapacity for pool waiting queue
     * @throws IOException if an I/O exception of some sort has occurred
     */
    public TcpServer(SocketAddress local, int backlog, int selectorsLength, int poolSize, int queueCapacity) throws IOException {
        super(TcpServer.class.getName(), poolSize, queueCapacity);
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.bind(local, backlog);

        selectors = new ServerSelector[selectorsLength];
        for (selectorsIndex = 0; selectorsIndex < selectorsLength; ++selectorsIndex)
            selectors[selectorsIndex] = new ServerSelector(ServerSelector.class.getName() + " " + selectorsIndex);
        selectorsIndex = 0;

        start();
        Logger.getLogger("org.appconn").log(Level.INFO, "bound local " + local);
    }

    @Override
    public void start() {
        for (ServerSelector selector : selectors) selector.start();
        super.start();
    }

    @Override
    public void run() {
        while (true) {
            try {
                SocketChannel channel = serverSocketChannel.accept();
                channel.configureBlocking(false);
                channel.socket().setTcpNoDelay(true);

                selectors[selectorsIndex++].register(channel, new ServerHandler(channel));
                if (selectorsIndex == selectors.length) selectorsIndex = 0;
            } catch (AsynchronousCloseException e) {
                break;
            } catch (IOException e) {
                Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                break;
            }
        }
    }

    @Override
    public void interrupt() {
        for (ServerSelector selector : selectors) selector.interrupt();
        super.interrupt();
        Logger.getLogger("org.appconn").log(Level.INFO, "interrupted");
    }

    private class ServerHandler implements Handler {

        private final SocketChannel channel;
        private final ReadBuffer readBuffer;
        private final WriteBuffer writeBuffer;

        private ServerHandler(SocketChannel channel) {
            this.channel = channel;
            readBuffer = new ReadHeapByteBuffer(channel, ReadBuffer.DEFAULT_LENGTH);
            writeBuffer = new WriteHeapByteBuffer(channel, WriteBuffer.DEFAULT_LENGTH);
        }

        @Override
        public synchronized void handle() {
            try {
                readBuffer.read();
                while (readBuffer.hasRemaining()) {
                    int key = readBuffer.getInt();
                    MethodStub stub = stubMap.get(key);
                    if (stub != null) {
                        int position = readBuffer.getInt();
                        executorService.execute(stub.runnable(readBuffer, writeBuffer, position));
                    } else {
                        Logger.getLogger("org.appconn").log(Level.WARNING, "null stub key " + key);
                        channel.close();
                    }
                }
            } catch (Exception e) {
                Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
            }
        }
    }
}
