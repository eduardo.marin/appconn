package org.appconn.channel;

import java.util.Arrays;

/**
 * <i>appconn</i> ReturnsArray implementation.
 */
final class ReturnsArray implements Returns {

    private static final int INITIAL_LENGTH = 1 << 12;      // 4096
    private static final int MAX_LENGTH = 1 << 20;          // 1048576
    private static final int DEFAULT_MAX_LENGTH = 1 << 16;  // 65536

    private final int maxLength;
    private MethodReturn<?>[] methodReturns;
    private int position;

    /**
     * Constructs a new ReturnsArray with the given maxLength.
     */
    ReturnsArray(int maxLength) {
        this.maxLength = maxLength < INITIAL_LENGTH ? INITIAL_LENGTH
                                                    : maxLength < MAX_LENGTH ? maxLength
                                                                             : MAX_LENGTH;
        methodReturns = new MethodReturn[INITIAL_LENGTH];
        position = 0;
    }

    /**
     * Constructs a new ReturnsArray with the default maxLength (65536).
     */
    ReturnsArray() {
        this(DEFAULT_MAX_LENGTH);
    }

    @Override
    public synchronized int add(MethodReturn<?> methodReturn) throws ChannelException {
        int tmpPosition = nextNull();
        if (tmpPosition == methodReturns.length) {
            int newLength = methodReturns.length << 1;
            if (newLength < maxLength) methodReturns = Arrays.copyOf(methodReturns, newLength);
            else if (methodReturns.length != maxLength) methodReturns = Arrays.copyOf(methodReturns, maxLength);
            else throw new ChannelException("methodReturns maxLength");
        }
        methodReturns[tmpPosition] = methodReturn;
        position = tmpPosition < methodReturns.length - 1 ? tmpPosition + 1 : 0;
        return tmpPosition;
    }

    @Override
    public synchronized MethodReturn<?> remove(int position) {
        MethodReturn<?> methodReturn = methodReturns[position];
        methodReturns[position] = null;
        return methodReturn;
    }

    private int nextNull() {
        int tmpPosition = position;
        for (int i = 0; i < methodReturns.length; ++i) {
            MethodReturn<?> methodReturn = methodReturns[tmpPosition];
            if (methodReturn == null) return tmpPosition;
            else if (methodReturn.getDeadline() != null && methodReturn.getDeadline() - System.nanoTime() < 0) {
                methodReturns[tmpPosition] = null;
                return tmpPosition;
            }
            if (tmpPosition < methodReturns.length - 1) ++tmpPosition;
            else tmpPosition = 0;
        }
        return methodReturns.length;
    }
}
