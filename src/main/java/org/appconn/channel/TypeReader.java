package org.appconn.channel;

import java.io.IOException;
import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.*;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Properties;

/**
 * <i>appconn</i> TypeReader functional interface.
 * @param <T> the type
 */
@FunctionalInterface
public interface TypeReader<T> {

    /**
     * Gets a type.
     * @param r the ReadBuffer
     * @return the type
     * @throws IOException if an I/O exception of some sort has occurred
     */
    T get(ReadBuffer r) throws IOException;

    /**
     * Gets an <i>appconn</i> type.
     * @param readBuffer the ReadBuffer
     * @param typeReader the TypeReader
     * @param <T>        the type
     * @return the type
     * @throws IOException if an I/O exception of some sort has occurred
     */
    static <T> T getType(ReadBuffer readBuffer, TypeReader<T> typeReader) throws IOException {
        return readBuffer.get() == Type.IS_NOT_NULL ? typeReader.get(readBuffer) : null;
    }

    /**
     * Gets a type array.
     * @param readBuffer the ReadBuffer
     * @param type       the type Class
     * @param typeReader the TypeReader
     * @param <T>        the type
     * @return the type array
     * @throws IOException if an I/O exception of some sort has occurred
     */
    @SuppressWarnings("unchecked")
    static <T> T[] getTypes(ReadBuffer readBuffer, Class<T> type, TypeReader<T> typeReader) throws IOException {
        if (readBuffer.get() == Type.IS_NOT_NULL) {
            T[] types = (T[]) Array.newInstance(type, readBuffer.getInt());
            for (int i = 0; i < types.length; ++i) types[i] = getType(readBuffer, typeReader);
            return types;
        } else return null;
    }

    /**
     * Gets a type Collection.
     * @param readBuffer the ReadBuffer
     * @param type       the type Class
     * @param typeReader the TypeReader
     * @param <T>        the type
     * @return the type collection
     * @throws IOException if an I/O exception of some sort has occurred
     */
    static <T> Collection<T> getCollection(ReadBuffer readBuffer, Class<T> type, TypeReader<T> typeReader) throws IOException {
        T[] types = getTypes(readBuffer, type, typeReader);
        if (types != null) return Arrays.asList(types);
        else return null;
    }

    TypeReader<byte[]> BYTES_READER = r -> getType(r, ReadBuffer::getBytes);

    TypeReader<Boolean> BOOLEAN_READER = r -> getType(r, ReadBuffer::getBoolean);
    TypeReader<Byte> BYTE_READER = r -> getType(r, ReadBuffer::get);
    TypeReader<Character> CHARACTER_READER = r -> getType(r, ReadBuffer::getChar);
    TypeReader<Double> DOUBLE_READER = r -> getType(r, ReadBuffer::getDouble);
    TypeReader<Float> FLOAT_READER = r -> getType(r, ReadBuffer::getFloat);
    TypeReader<Integer> INTEGER_READER = r -> getType(r, ReadBuffer::getInt);
    TypeReader<Long> LONG_READER = r -> getType(r, ReadBuffer::getLong);
    TypeReader<Short> SHORT_READER = r -> getType(r, ReadBuffer::getShort);
    TypeReader<String> STRING_READER = r -> getType(r, ReadBuffer::getString);

    TypeReader<Void> VOID_READER = r -> null;

    TypeReader<BigDecimal> BIG_DECIMAL_READER = readBuffer -> getType(readBuffer, r -> new BigDecimal(new BigInteger(r.getBytes()),
                                                                                                      r.getInt()));
    TypeReader<BigInteger> BIG_INTEGER_READER = readBuffer -> getType(readBuffer, r -> new BigInteger(r.getBytes()));

    TypeReader<Date> DATE_READER = readBuffer -> getType(readBuffer, r -> new Date(r.getLong()));

    TypeReader<Instant> INSTANT_READER = readBuffer -> getType(readBuffer, r -> Instant.ofEpochSecond(r.getLong(), r.getLong()));
    TypeReader<LocalDate> LOCAL_DATE_READER = readBuffer -> getType(readBuffer, r -> LocalDate.of(r.getInt(), r.getInt(), r.getInt()));
    TypeReader<LocalDateTime> LOCAL_DATE_TIME_READER = readBuffer -> getType(readBuffer, r -> LocalDateTime.of(r.getInt(), r.getInt(), r.getInt(), r.getInt(), r.getInt(), r.getInt(), r.getInt()));
    TypeReader<LocalTime> LOCAL_TIME_READER = readBuffer -> getType(readBuffer, r -> LocalTime.of(r.getInt(), r.getInt(), r.getInt(), r.getInt()));
    TypeReader<ZonedDateTime> ZONED_DATE_TIME_READER = readBuffer -> getType(readBuffer, r -> ZonedDateTime.of(r.getInt(), r.getInt(), r.getInt(), r.getInt(), r.getInt(), r.getInt(), r.getInt(), ZoneId.of(r.getString())));

    TypeReader<Type.StringsEntry> STRINGS_ENTRY_READER = readBuffer -> getType(readBuffer, r -> new Type.StringsEntry(STRING_READER.get(r),
                                                                                                                      STRING_READER.get(r)));

    TypeReader<Properties> PROPERTIES_READER = readBuffer -> getType(readBuffer, r -> {
        Collection<Type.StringsEntry> entries = getCollection(r, Type.StringsEntry.class, STRINGS_ENTRY_READER);
        Properties properties = new Properties();
        if (entries != null)
            for (Type.StringsEntry entry : entries) properties.setProperty(entry.getKey(), entry.getValue());
        return properties;
    });

    TypeReader<StackTraceElement> STACK_TRACE_ELEMENT_READER = readBuffer -> getType(readBuffer, r -> new StackTraceElement(STRING_READER.get(r),
                                                                                                                            STRING_READER.get(r),
                                                                                                                            STRING_READER.get(r),
                                                                                                                            r.getInt()));

    TypeReader<Throwable> THROWABLE_READER = readBuffer -> getType(readBuffer, r -> {
        Throwable throwable = new Throwable(STRING_READER.get(r));
        StackTraceElement[] stackTrace = getTypes(r, StackTraceElement.class, STACK_TRACE_ELEMENT_READER);
        if (stackTrace != null) throwable.setStackTrace(stackTrace);
        return throwable;
    });
}
