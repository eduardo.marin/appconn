package org.appconn.channel;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.WritableByteChannel;

/**
 * <i>appconn</i> WriteBuffer <em>DirectByteArray</em> implementation.
 */
final class WriteDirectByteArrayBuffer extends WriteHeapByteBuffer {

    WriteDirectByteArrayBuffer(WritableByteChannel channel, int length) {
        super(channel);
        buffer = new byte[length];
        byteBuffer = ByteBuffer.allocateDirect(length);
        byteBuffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    public void flush() throws IOException {
        byteBuffer.put(buffer, 0, position);
        byteBuffer.flip();
        while (byteBuffer.hasRemaining()) channel.write(byteBuffer);
        position = 0;
        byteBuffer.clear();
    }
}
