package org.appconn.channel;

/**
 * <i>appconn</i> channel Handler.
 */
interface Handler {

    void handle();
}
