package org.appconn.channel;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * <i>appconn</i> FutureReturn implementation.
 * @param <V> the return type
 */
class MethodReturn<V> implements FutureReturn<V> {

    private final TypeReader<V> reader;
    private V returnValue;
    private Exception exception;
    private final Long deadline;

    private boolean wait;

    /**
     * Constructs a new MethodReturn.
     * @param reader  the return TypeReader
     * @param timeout the timeout in nanos
     */
    MethodReturn(TypeReader<V> reader, Long timeout) {
        this.reader = reader;
        returnValue = null;
        exception = null;
        deadline = timeout != null ? System.nanoTime() + timeout : null;
        wait = true;
    }

    @Override
    public boolean isDone() {
        return returnValue != null || exception != null;
    }

    @Override
    public synchronized V getReturn() throws Exception {
        while (wait) {
            if (deadline != null) {
                long timeout = deadline - System.nanoTime();
                if (timeout > 0) TimeUnit.NANOSECONDS.timedWait(this, timeout);
                else setException(new TimeoutException("timeout"));
            } else wait();
        }
        if (exception != null) throw exception;
        else return returnValue;
    }

    /**
     * Sets the return.
     * @param r the ReadBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    synchronized void setReturn(ReadBuffer r) throws IOException {
        this.returnValue = reader.get(r);
        wait = false;
        notify();
    }

    /**
     * Sets the Exception.
     */
    synchronized void setException(Exception exception) {
        this.exception = exception;
        wait = false;
        notify();
    }

    /**
     * @return the deadline
     */
    Long getDeadline() {
        return deadline;
    }
}
