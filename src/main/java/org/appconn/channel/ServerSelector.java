package org.appconn.channel;

import java.io.IOException;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <i>appconn</i> ServerSelector.
 */
final class ServerSelector extends Thread {

    private final Selector selector;

    /**
     * Constructs a new ServerSelector.
     * @param name the name of the thread
     * @throws IOException if an I/O exception of some sort has occurred
     */
    ServerSelector(String name) throws IOException {
        super(name);
        selector = Selector.open();
    }

    @Override
    public void run() {
        while (true) {
            try {
                selector.select();
                Iterator<SelectionKey> iterator = selector.selectedKeys().iterator();
                while (iterator.hasNext()) {
                    SelectionKey selectionKey = iterator.next();
                    iterator.remove();
                    ((Handler) selectionKey.attachment()).handle();
                }
                synchronized (this) { }
            } catch (IOException e) {
                Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
                break;
            } catch (ClosedSelectorException e) {
                break;
            }
        }
    }

    @Override
    public void interrupt() {
        try {
            selector.close();
        } catch (IOException e) {
            Logger.getLogger("org.appconn").log(Level.CONFIG, e, e::getMessage);
        }
        super.interrupt();
    }

    /**
     * Register the given channel and handler.
     * @param channel the SocketChannel
     * @param handler the Handler
     * @throws IOException if an I/O exception of some sort has occurred
     */
    synchronized void register(SocketChannel channel, Handler handler) throws IOException {
        selector.wakeup();
        channel.register(selector, SelectionKey.OP_READ, handler);
        Logger.getLogger("org.appconn").log(Level.FINE, getName() + " " + channel.getRemoteAddress());
    }
}
