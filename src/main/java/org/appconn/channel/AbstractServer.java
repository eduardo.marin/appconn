package org.appconn.channel;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <i>appconn</i> AbstractServer.
 */
public abstract class AbstractServer extends Thread {

    public static final int DEFAULT_POOL_SIZE = 4;
    public static final int DEFAULT_QUEUE_CAPACITY = 1 << 8; // 256

    final Map<Integer, MethodStub> stubMap;

    final ExecutorService executorService;

    /**
     * AbstractServer constructor.
     * @param name          the name of the thread
     * @param poolSize      the poolSize for ThreadPoolExecutor
     * @param queueCapacity the queueCapacity for the pool waiting queue
     */
    AbstractServer(String name, int poolSize, int queueCapacity) {
        super(name);
        stubMap = new HashMap<>();
        executorService = new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(queueCapacity), new ServerThreadFactory(getClass().getName()));
    }

    @Override
    public void interrupt() {
        executorService.shutdown();
        super.interrupt();
    }

    /**
     * Puts the stub.
     * @param key  the stub key
     * @param stub the MethodStub
     */
    public void put(int key, MethodStub stub) {
        if (Optional.ofNullable(stubMap.put(key, stub)).isPresent())
            Logger.getLogger("org.appconn").log(Level.WARNING, "replace " + stub.getClass().getName() + "/" + key);
        else Logger.getLogger("org.appconn").log(Level.CONFIG, stub.getClass().getName() + "/" + key);
    }

    private class ServerThreadFactory implements ThreadFactory {

        private final ThreadGroup group;
        private final AtomicInteger index;
        private final String name;

        private ServerThreadFactory(String name) {
            SecurityManager securityManager = System.getSecurityManager();
            group = (securityManager != null) ? securityManager.getThreadGroup() : Thread.currentThread().getThreadGroup();
            index = new AtomicInteger(0);
            this.name = name;
        }

        @Override
        public Thread newThread(Runnable r) {
            Thread t = new Thread(group, r, name + " " + index.getAndIncrement());
            if (t.isDaemon()) t.setDaemon(false);
            if (t.getPriority() != Thread.NORM_PRIORITY) t.setPriority(Thread.NORM_PRIORITY);
            return t;
        }
    }
}
