package org.appconn.channel;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <i>appconn</i> Writer functional interface.
 */
@FunctionalInterface
public interface Writer {

    byte IS_RETURN = 0;
    byte IS_EXCEPTION = 1;

    Writer VOID = w -> { };

    /**
     * @param w the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    void put(WriteBuffer w) throws IOException;

    /**
     * Puts a method call.
     * @param writeBuffer the WriteBuffer
     * @param key         the method key
     * @param position    the FutureReturn position
     * @param ret         the FutureReturn
     * @param writer      the Writer
     */
    static void putCall(WriteBuffer writeBuffer, int key, int position, FutureReturn<?> ret, Writer writer) {
        writeBuffer.lock();
        try {
            writeBuffer.putInt(key).putInt(position);
            writer.put(writeBuffer);
            writeBuffer.flush();
        } catch (IOException e) {
            ((MethodReturn<?>) ret).setException(e);
        } finally {
            writeBuffer.unlock();
        }
    }

    /**
     * Puts a method return.
     * @param writeBuffer the WriteBuffer
     * @param position    the position
     * @param deadline    the deadline
     * @param writer      the Writer
     */
    static void putReturn(WriteBuffer writeBuffer, int position, Long deadline, Writer writer) {
        writeBuffer.lock();
        try {
            if (deadline == null || deadline - System.nanoTime() > 0) {
                writeBuffer.putInt(position);
                writeBuffer.put(IS_RETURN);
                writer.put(writeBuffer);
                writeBuffer.flush();
            } else Logger.getLogger("org.appconn").log(Level.WARNING, "timeout position " + position);
        } catch (IOException e) {
            Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
        } finally {
            writeBuffer.unlock();
        }
    }

    /**
     * Puts an exception.
     * @param writeBuffer the WriteBuffer
     * @param position    the position
     * @param deadline    the deadline
     * @param exception   the Exception
     */
    static void putException(WriteBuffer writeBuffer, int position, Long deadline, Exception exception) {
        writeBuffer.lock();
        try {
            if (deadline == null || deadline - System.nanoTime() > 0) {
                writeBuffer.putInt(position);
                writeBuffer.put(IS_EXCEPTION);
                TypeWriter.STRING_WRITER.put(writeBuffer, exception.getClass().getName());
                TypeWriter.THROWABLE_WRITER.put(writeBuffer, exception);
                writeBuffer.flush();
            } else Logger.getLogger("org.appconn").log(Level.WARNING, "timeout position " + position);
        } catch (IOException e) {
            Logger.getLogger("org.appconn").log(Level.WARNING, e, e::getMessage);
        } finally {
            writeBuffer.unlock();
        }
    }
}
