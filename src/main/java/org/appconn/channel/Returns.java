package org.appconn.channel;

/**
 * <i>appconn</i> Returns interface.
 */
interface Returns {

    /**
     * Adds the given MethodReturn.
     * @return the MethodReturn position
     * @throws ChannelException if a ChannelException of some sort has occurred
     */
    int add(MethodReturn<?> methodReturn) throws ChannelException;

    /**
     * Remove the MethodReturn at given position.
     * @return the removed MethodReturn
     */
    MethodReturn<?> remove(int position);
}
