package org.appconn.channel;

import java.io.IOException;

/**
 * <i>appconn</i> WriteBuffer interface.
 */
public interface WriteBuffer {

    int DEFAULT_LENGTH = 1 << 12;   // 4096
    int SMALL_LENGTH = 1 << 10;     // 1024
    String CHARSET_NAME = "UTF-8";

    /**
     * Puts a byte.
     * @param value the byte value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer put(byte value) throws IOException;

    /**
     * Puts a boolean.
     * @param value the boolean value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    default WriteBuffer putBoolean(boolean value) throws IOException {
        return put(value ? (byte) 1 : (byte) 0);
    }

    /**
     * Puts a byte array.
     * @param value the byte array
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer putBytes(byte[] value) throws IOException;

    /**
     * Puts a char.
     * @param value the char value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer putChar(char value) throws IOException;

    /**
     * Puts a double.
     * @param value the double value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer putDouble(double value) throws IOException;

    /**
     * Puts a float
     * @param value the float value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer putFloat(float value) throws IOException;

    /**
     * Puts an int.
     * @param value the int value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer putInt(int value) throws IOException;

    /**
     * Puts a long.
     * @param value the long value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer putLong(long value) throws IOException;

    /**
     * Puts a short.
     * @param value the short value.
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    WriteBuffer putShort(short value) throws IOException;

    /**
     * Puts a String.
     * @param value the String value
     * @return the WriteBuffer
     * @throws IOException if an I/O exception of some sort has occurred
     */
    default WriteBuffer putString(String value) throws IOException {
        return putBytes(value.getBytes(CHARSET_NAME));
    }

    /**
     * Flushes the WriteBuffer.
     * @throws IOException if an I/O exception of some sort has occurred
     */
    void flush() throws IOException;

    /**
     * Locks the WriteBuffer.
     */
    void lock();

    /**
     * Unlocks the WriteBuffer.
     */
    void unlock();
}
