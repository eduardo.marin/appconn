package org.appconn.channel;

import java.io.IOException;

/**
 * <i>appconn</i> ReadBuffer interface.
 */
public interface ReadBuffer {

    int DEFAULT_LENGTH = 1 << 12;   // 4096
    int SMALL_LENGTH = 1 << 10;     // 1024

    /**
     * @return the byte at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    byte get() throws IOException;

    /**
     * @return the boolean at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    default boolean getBoolean() throws IOException {
        return get() != 0;
    }

    /**
     * @return the byte array at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    byte[] getBytes() throws IOException;

    /**
     * @return the char at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    char getChar() throws IOException;

    /**
     * @return the double at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    double getDouble() throws IOException;

    /**
     * @return the float at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    float getFloat() throws IOException;

    /**
     * @return the int at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    int getInt() throws IOException;

    /**
     * @return the long at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    long getLong() throws IOException;

    /**
     * @return the short at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    short getShort() throws IOException;

    /**
     * @return the String at current position
     * @throws IOException if an I/O exception of some sort has occurred
     */
    String getString() throws IOException;

    /**
     * @return true if it has remaining
     */
    boolean hasRemaining();

    /**
     * @return the number of bytes read
     * @throws IOException if an I/O exception of some sort has occurred
     */
    int read() throws IOException;
}
